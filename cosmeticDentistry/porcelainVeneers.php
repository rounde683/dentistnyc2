<?php
	$pageTitle = "Dr. Inna Chern: Cosmetic Dentistry - Porcelain Veneers";
	$pageKeywords = "manhattan porcelain veneers,manhattan porcelain veneer dentist,NY porcelain veneers,NY porcelain veneer dentist,NYC porcelain veneers,NYC porcelain veneer dentist,new york porcelain veneers,new york porcelain veneer dentist,new york city porcelain veneers,new york city porcelain veneer dentist,";
	$pageDesc = "Dr. Inna Chern discusses what to expect with Porcelain Veneers.";
	require_once("../tehPHP/dentHeader.php");
?>
<div class="dentistBG whiteText">
	<div class="centerWrap whiteBG stdBoxShadow contentShell" style="min-height: 400px;">	
		<div class="mainPageDirectionsShell">
			<div class="centerWrap">
				<div class="contentTitle">
					Manhattan Porcelain Veneers
				</div>
				<p class="grayText">
					We will take photographs and have an extensive consultation to discuss your desires and expectations. Bringing pictures to this appointment is a great idea. We will take impressions and order a waxup and stent to show the finished product and discuss any changes to the teeth.
				</p>
				<div class="contentSubTitle">
					SECOND APPOINTMENT
				</div>
				<p class="grayText">
					We schedule another appointment to show you the waxup and discuss the necessary changes. The stent allows us to place the waxup in your mouth so you can truly visualize the final result. If you are happy with the outcome, we start working to prepare the teeth for the veneers. Anesthetic is used to numb the area and the teeth are prepared. Impressions are taken and a temporary is placed so you can get used to the new shape and feel of the veneers.
				</p>
				<div class="contentSubTitle">
					THIRD APPOINTMENT
				</div>
				<p class="grayText">
					The veneers are tried in, evaluated for fit and aesthetics. Once all the necessary tests are performed to make sure function is ideal, they are cemented. The area is flossed and cleaned. Post insertion instructions are given and we schedule a followup appointment in 2 weeks.
				</p>
				<div class="contentSubTitle">
					FOURTH APPOINTMENT (TWO WEEKS LATER)
				</div>
				<p class="grayText">
					We reevaluate speech, function, occlusion and aesthetics. Impressions are taken for a nightguard appliance to protect the veneers. The appliance is delivered one week later.
				</p>
			</div>
		</div>
	</div>
</div>



<?php
	require_once("../tehPHP/dentFooter.php");
?>