<?php
	//$tehAbsoluteURL = "http://localhost/dentistnyc2/";
	$pageTitle = "Dr. Inna Chern: Manhattan Porcelain Crowns &amp; Bridges";
	$pageKeywords = "";
	$pageDesc = "Manhattan Porcelain Crowns &amp; Bridges discussed by Inna Chern DDS.";
	require_once("../tehPHP/dentHeader.php");
?>

<div class="dentistBG whiteText">
	<div class="centerWrap whiteBG stdBoxShadow contentShell" style="min-height: 400px;">	
		<div class="mainPageDirectionsShell">
			<div class="centerWrap">
				<div class="contentTitle">
					Manhattan Porcelain Crowns &amp; Bridges
				</div>
				<div class="contentSubTitle">
					Brightening the Smiles of Manhattan and Brooklyn
				</div>
			</div>
		</div>
	</div>
</div>

<?php
	require_once("../tehPHP/dentFooter.php");
?>