<?php
	$pageTitle = "Dr. Inna Chern: Cosmetic Dentistry - Invisalign";
	$pageKeywords = "manhattan invisalign, manhattan invisalign dentist, manhattan invisalign procedure, manhattan invisalign dentistry,NY invisalign, NY invisalign dentist, NY invisalign procedure, NY invisalign dentistry,NYC invisalign, NYC invisalign dentist, NYC invisalign procedure, NYC invisalign dentistry,new york invisalign, new york invisalign dentist, new york invisalign procedure, new york invisalign dentistry,new york city invisalign, new york city invisalign dentist, new york city invisalign procedure, new york city invisalign dentistry";
	$pageDesc = "Dr. Inna Chern discusses who should consider Dental Implants what to expect for dental implant procedures.";
	require_once("../tehPHP/dentHeader.php");
?>
<div class="dentistBG whiteText">
	<div class="centerWrap whiteBG stdBoxShadow contentShell" style="min-height: 400px;">	
		<div class="mainPageDirectionsShell">
			<div class="centerWrap">
				<div class="contentTitle">
					Manhattan Invisalign
				</div>
				<p class="grayText">
					Straighter teeth are happy and healthy teeth. If you want a straighter smile without the metal hardware you may seen when you were a child, Invisalign is the answer!
				</p>
				<p class="grayText">
					Daily habits over time can put a toll on your teeth. Coffee, tea, soda and tobacco use can darken the appearance of your teeth. Its as simple as an hour in our office and we can remove years of stain damage with several great options. A brighter smile is just around the corner.
				</p>
				<div class="contentTitle">
					WHY CHOOSE INVISALIGN?
				</div>
				<p class="grayText">
					Invisalign Teen and Invisalign Adult are a proven way to achieve healthy tooth movement with ease of brushing and flossing. The clear aligners are undetectable in the mouth
				</p>
				<div class="contentTitle">
					MOST PEOPLE ARE GREAT CANDIDATES FOR THIS TREATMENT:
				</div>
				<ul class="contentList grayText">
					<li>Fixes crowded and croaked teeth</li>
					<li>Closes gaps between teeth</li>
					<li>Rotates teeth for proper alignment</li>
					<li>Widens narrow smiles to give a fuller appearance</li>
				</ul>

				<div class="contentTitle">
					WHAT TO EXPECT
				</div>
				<p class="grayText">
					After an exam with radiographs and eradication of any teeth with cavities, Dr. Chern will talk with you about the issues you have with your smile and any issues she sees which may be functionally problematic. Photos will be taken of your teeth and smile along with impressions of your upper and lower teeth.
				</p>
				<p class="grayText">
					These records will be sent to Invisalign where they will create a digital model of your mouth. In 1-2 weeks, your unique Clincheck is ready for discussion. The ClinCheck is a 3D simulation of the movement planned in your treatment. This amazing program will show you presisely how the teeth will move and the time it will take to achieve the case. Once you are happy with the results, we order the trays and you can begin your journey to a healthier and happier smile.
				<p class="grayText">
					The trays are worn for 10-14 days for 20-22 hours a day. In general, we like to see you once a month to evaluate how the trays are fitting and check for proper movement.
				</p>
				<p class="grayText">
					Included in a full Invisalign treatment are  refinements. A refinement will help fine tune any small corrections that are needed. When treatment is complete, we fabricate a retainer and schedule follow-up appointments. 
				</p>
			</div>
		</div>
	</div>
</div>



<?php
	require_once("../tehPHP/dentFooter.php");
?>
?>