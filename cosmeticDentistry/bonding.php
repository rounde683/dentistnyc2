<?php
	$pageTitle = "Dr. Inna Chern: Cosmetic Dentistry - Bonding";
	$pageKeywords = "manhattan dental bonding, manhattan bonding dentist, manhattan bonding dentistry,NY dental bonding, NY bonding dentist, NY bonding dentistry,NYC dental bonding, NYC bonding dentist, NYC bonding dentistry,new york dental bonding, new york bonding dentist, new york bonding dentistry,new york city dental bonding, new york city bonding dentist, new york city bonding dentistry";
	$pageDesc = "Cosmetic Bonding services of Manhattan. Dr. Inna Chern discusses what to expect from Dental Cosmetic Bonding";
	require_once("../tehPHP/dentHeader.php");
?>
<div class="dentistBG whiteText">
	<div class="centerWrap whiteBG stdBoxShadow contentShell" style="min-height: 400px;">	
		<div class="mainPageDirectionsShell">
			<div class="centerWrap">
				<div class="contentTitle">
					Manhattan Dental Bonding
				</div>
				<p class="grayText">
					"There are small imperfections like chips, discolorations and gaps in your teeth and you are not ready to commit to veneers or crowns but a great alternative may be cosmetic bonding!."
				</p>
				<p class="grayText">
					Tooth colored fillings can be used to help reshape, enhance and restore missing or damaged tooth structure. With a wide variety of shades and hues, Dr. Chern can mix and match shades to blend bonding material to blend with your natural teeth or better yet enhance them. A lot of this work can be done without drilling teeth and is a very good conservative option.
				</p>
				<div class="contentSubTitle">
					WHAT TO EXPECT
				</div>
				<div class="contentSubTitleCaption grayText">
					1-2 appointments varying in time from 45 to 90 minutes
				</div>
				<p class="grayText">
					Photos may be taken before we start to discuss treatment expectations and goals. If we’re looking for a cosmetic enhancement, we will need to take impressions and measurements of the teeth. A lab waxup is ordered so you can visualize the final product and make any corrections to meet your desires.
				</p>
				<p class="grayText">
					We may or may not get the area numb and a small level of polishing the tooth surface is done prior to Dr. Chern choosing and blending several different shades to recreate your custom tooth shade and hue. The result will then be placed on the tooth. The tooth is then contoured and polished for a natural appearance.
				</p>
				<p class="grayText">
					Once you are happy with the outcome and the bite is checked and adjusted, we will give you instructions on eating and possibly recommend a nightguard to prevent further damage and breakage.
				</p>
				<p class="grayText">
					Composite bondings can last for years as long as care is taken and upkeep is performed with routine dental visits.
				</p>
			</div>
		</div>
	</div>
</div>



<?php
	require_once("../tehPHP/dentFooter.php");
?>