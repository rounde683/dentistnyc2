<?php
	//$tehAbsoluteURL = "http://localhost/dentistnyc2/";
	$pageTitle = "Dr. Inna Chern: Manhattan Cosmetic Dental Services &amp; Brooklyn Cosmetic Dental Services";
	$pageKeywords = "";
	$pageDesc = "Manhattan Cosmetic Dental Services. Learn about our Cosmetic Dental services and schedule an appointment now.";
	require_once("../tehPHP/dentHeader.php");
?>

<div class="dentistBG whiteText">
	<div class="centerWrap whiteBG stdBoxShadow contentShell" style="min-height: 400px;">	
		<div class="mainPageDirectionsShell">
			<div class="centerWrap">
				<div class="contentTitle">
					Cosmetic Dental Services
				</div>
				<div class="contentSubTitle">
					Brightening the Smiles of Manhattan and Brooklyn
				</div>
			</div>
		</div>
	</div>
</div>


<?php
	require_once("../tehPHP/dentFooter.php");
?>