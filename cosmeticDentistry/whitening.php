<?php
	$pageTitle = "Dr. Inna Chern: Cosmetic Dentistry - Whitenings";
	$pageKeywords = "manhattan whitening, manhattan whitenings, manhattan whitening treament, manhattan whitening treatments, manhattan teeth whitening, manhattan teeth whitenings, manhattan dental whitening, manhattan dental whitenings,NY whitening, NY whitenings, NY whitening treament, NY whitening treatments, NY teeth whitening, NY teeth whitenings, NY dental whitening, NY dental whitenings,NYC whitening, NYC whitenings, NYC whitening treament, NYC whitening treatments, NYC teeth whitening, NYC teeth whitenings, NYC dental whitening, NYC dental whitenings,new york whitening, new york whitenings, new york whitening treament, new york whitening treatments, new york teeth whitening, new york teeth whitenings, new york dental whitening, new york dental whitenings,new york city whitening, new york city whitenings, new york city whitening treament, new york city whitening treatments, new york city teeth whitening, new york city teeth whitenings, new york city dental whitening, new york city dental whitenings";
	$pageDesc = "Dr. Inna Chern discusses who should consider Dental Implants what to expect for dental implant procedures.";
	require_once("../tehPHP/dentHeader.php");
?>
<div class="dentistBG whiteText">
	<div class="centerWrap whiteBG stdBoxShadow contentShell" style="min-height: 400px;">	
		<div class="mainPageDirectionsShell">
			<div class="centerWrap">
				<div class="contentTitle">
					MANHATTAN WHITENINGS
				</div>
				<p class="grayText">
					Do you wish your smile was whiter and brighter. Years of coffee, wine or an old cigarette habit have your teeth a bit yellow. We have the solution for you so you can turn your frown upside down!
				</p>
				<p class="grayText">
					Daily habits over time can put a toll on your teeth. Coffee, tea, soda and tobacco use can darken the appearance of your teeth. Its as simple as an hour in our office and we can remove years of stain damage with several great options. A brighter smile is just around the corner.
				</p>
				<div class="contentTitle">
					WHAT TO EXPECT
				</div>
				<p class="grayText">
					After an exam,cleaning and any necessary dental work have been completed, Dr Chern will evaluate the teeth for any sensitivie areas and discuss two great options for whitening. If your teeth are sensitive, we recommend using Sensodyne or Prevident for 10 days before treatment. We have an in office 1 hour treatment which can be performed as soon as the cleaning is completed or we can customize trays for your teeth which hold a gel solution.
				</p>
				<p class="grayText">
					The in office treatment consists of 3 intervals of about 15 minutes. Once the treatment is complete, Dr. Chern provides you with instructions to maintain the stain-free smile.
				</p>
					The trays are made by taking putty impressions of the teeth. They are then sent to a lab and fabricated to fit your teeth and allow space for the bleaching solution. The second appointment which s approximately one week later is when Dr. Chern shows you how to use the trays. This treatment can be done at home for up to 2 weeks , several times a year.
				</p>
				<p class="grayText">
					Brief sensitivity may occur to even the most non-sensitive people, but will not last long. Brushing with toothpastes like Sensodyne or Prevident. Symptoms usually resolve between 24-48 hours.
				</p>
			</div>
		</div>
	</div>
</div>



<?php
	require_once("../tehPHP/dentFooter.php");
?>
?>