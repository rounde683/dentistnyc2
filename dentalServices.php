<?php
	//$tehAbsoluteURL = "http://localhost/dentistnyc2/";
	$pageTitle = "Dr. Inna Chern: Manhattan Dental Services &amp; Brooklyn Dental Services";
	$pageKeywords = "manhattan dentist service, manhattan dental service, manhattan preventative dental service, manhattan preventative dentist service, manhattan restorative dentist service, manhattan restorative dental service, manhattan cosmetic dentist service, manhattan cosmetic dental service, manhattan dentist implant service, manhattan dental implant service,manhattan dentist services, manhattan dental services, manhattan preventative dental services, manhattan preventative dentist services, manhattan restorative dentist services, manhattan restorative dental services, manhattan cosmetic dentist services, manhattan cosmetic dental services, manhattan dentist implant services, manhattan dental implant services,brooklyn dentist services, brooklyn dental services, brooklyn preventative dental services, brooklyn preventative dentist services, brooklyn restorative dentist services, brooklyn restorative dental services, brooklyn cosmetic dentist services, brooklyn cosmetic dental services, brooklyn dentist implant services, brooklyn dental implant services, NYC dentist services, NYC dental services, NYC preventative dental services, NYC preventative dentist services, NYC restorative dentist services, NYC restorative dental services, NYC cosmetic dentist services, NYC cosmetic dental services, NYC dentist implant services, NYC dental implant services,NY dentist services, NY dental services, NY preventative dental services, NY preventative dentist services, NY restorative dentist services, NY restorative dental services, NY cosmetic dentist services, NY cosmetic dental services, NY dentist implant services, NY dental implant services,new york dentist services, new york dental services, new york preventative dental services, new york preventative dentist services, new york restorative dentist services, new york restorative dental services, new york cosmetic dentist services, new york cosmetic dental services, new york dentist implant services, new york dental implant services,new york city dentist services, new york city dental services, new york city preventative dental services, new york city preventative dentist services, new york city restorative dentist services, new york city restorative dental services, new york city cosmetic dentist services, new york city cosmetic dental services, new york city dentist implant services, new york city dental implant services";
	$pageDesc = "Manhattan Dental Services ranging from preventative care to dental implants and cosmetic.";
	require_once("tehPHP/dentHeader.php");
?>

<div class="dentistBG whiteText">
	<div class="centerWrap whiteBG stdBoxShadow contentShell" style="min-height: 400px;">	
		<div class="mainPageDirectionsShell">
			<div class="centerWrap">
				<div class="contentTitle">
					Our Dental Services
				</div>
				<div class="contentSubTitle">
					Brightening the Smiles of Manhattan and Brooklyn
				</div>
				<div class="gridShellThirtyThree" style="grid-template-columns: repeat(auto-fit, minmax(250px, 1fr));">
					<div class="gridColumnShell">
						<a href="<?php echo $tehAbsoluteURL; ?>preventativeDentistry/" class="dentalServiceIconShell">
							<div class="centerText">
								<img width="90" alt="" title="" src="<?php echo $tehAbsoluteURL; ?>layout/icons/dentalXrayIcon.png" />
							</div>
							<div class="dentsalServiceTitle">
								Preventative
							</div>
						</a>
						<ul class="contentServicesList">
							<li>
								<a href="<?php echo $tehAbsoluteURL; ?>preventativeDentistry/dentalExamAndCleaning.php">
									 Dental Exam &amp; Cleaning
								</a>
							</li>
							<li>
								<a href="<?php echo $tehAbsoluteURL; ?>preventativeDentistry/oralCancerScreenings.php">
									Oral Cancer Screening
								</a>
							</li>
							<li>
								<a href="<?php echo $tehAbsoluteURL; ?>preventativeDentistry/pediatricDentalTreatments.php">
									Pediatric Dental Treatments
								</a>
							</li>
							<li>
								<a href="<?php echo $tehAbsoluteURL; ?>preventativeDentistry/nightAndSportsGuards.php">
									Night &amp; Sport Guards
								</a>
							</li>
							<li>
								<a href="<?php echo $tehAbsoluteURL; ?>preventativeDentistry/periodontalTreatments.php">
									Periodontal Treatments
								</a>
							</li>
							<li>
								<a href="<?php echo $tehAbsoluteURL; ?>preventativeDentistry/dentalEmergencyCare.php">
									Dental Emergency Care
								</a>
							</li>
						</ul>
					</div>
					<div class="gridColumnShell">
						<a href="<?php echo $tehAbsoluteURL; ?>restorativeDentistry/" class="dentalServiceIconShell">
							<div class="centerText">
								<img width="90" alt="" title="" src="<?php echo $tehAbsoluteURL; ?>layout/icons/dentalPreventionIcon.png" />
							</div>
							<div class="dentsalServiceTitle">
								Restorative
							</div>
						</a>
						<ul class="contentServicesList">
							<li>
								<a href="<?php echo $tehAbsoluteURL; ?>restorativeDentistry/toothColoredFillings.php">
									  Tooth Colored Ceramic Fillings
								</a>
							</li>
							<li>
								<a href="<?php echo $tehAbsoluteURL; ?>restorativeDentistry/crownsAndBridges.php">
									Crowns &amp; Bridges
								</a>
							</li>
							<li>
								<a href="<?php echo $tehAbsoluteURL; ?>restorativeDentistry/rootCanals.php">
									Root Canals
								</a>
							</li>
							<li>
								<a href="<?php echo $tehAbsoluteURL; ?>restorativeDentistry/extractions.php">
									Extractions
								</a>
							</li>
							<li>
								<a href="<?php echo $tehAbsoluteURL; ?>restorativeDentistry/dentures.php">
									Dentures
								</a>
							</li>
						</ul>
					</div>
					<div class="gridColumnShell">
						<a href="<?php echo $tehAbsoluteURL; ?>cosmeticDentistry/" class="dentalServiceIconShell">
							<div class="centerText">
								<img width="90" alt="" title="" src="<?php echo $tehAbsoluteURL; ?>layout/icons/dentalWhiteningIcon.png" />
							</div>
							<div class="dentsalServiceTitle">
								Cosmetic
							</div>
						</a>
						<ul class="contentServicesList">
							<li>
								<a href="<?php echo $tehAbsoluteURL; ?>cosmeticDentistry/bonding.php">
									Bonding
								</a>
							</li>
							<li>
								<a href="<?php echo $tehAbsoluteURL; ?>cosmeticDentistry/porcelainVeneers.php">
									Veneers
								</a>
							</li>
							<li>
								<a href="<?php echo $tehAbsoluteURL; ?>cosmeticDentistry/porcelainCrowns.php">
									Porcelain Crowns &amp; Bridges
								</a>
							</li>
							<li>
								<a href="<?php echo $tehAbsoluteURL; ?>cosmeticDentistry/invisalign.php">
									Invisalign
								</a>
							</li>
							<li>
								<a href="<?php echo $tehAbsoluteURL; ?>cosmeticDentistry/whitening.php">
									Whitening Treatments
								</a>
							</li>
						</ul>
					</div>
					<div class="gridColumnShell">
						<a href="<?php echo $tehAbsoluteURL; ?>dentalImplants/" class="dentalServiceIconShell">
							<div class="centerText">
								<img width="90" alt="" title="" src="<?php echo $tehAbsoluteURL; ?>layout/icons/dentalImplantsIcon.png" />
							</div>
							<div class="dentsalServiceTitle">
								Implants
							</div>
						</a>
						<ul class="contentServicesList">
							<li>
								<a href="<?php echo $tehAbsoluteURL; ?>dentalImplants/dentalImplants.php">
									Implants
								</a>
							</li>
							<li>
								<a href="<?php echo $tehAbsoluteURL; ?>dentalImplants/implantCrowns.php">
									Implant Crowns
								</a>
							</li>
							<li>
								<a href="<?php echo $tehAbsoluteURL; ?>dentalImplants/implantBridges.php">
									Implant Bridges
								</a>
							</li>
							<li>
								<a href="<?php echo $tehAbsoluteURL; ?>dentalImplants/retainedDentures.php">
									Implant Retained Dentures
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>



<?php
	//if (substr_count(dirname($_SERVER['PHP_SELF']), '/') == "1")
	if( (substr_count(dirname($_SERVER['PHP_SELF']), '/') == "1") || (dirname($_SERVER['PHP_SELF']) == "/"))
	{
		require_once("./tehPHP/dentFooter.php");
	}
	else
	{
		require_once("../tehPHP/dentFooter.php");
	}
?>