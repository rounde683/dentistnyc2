<?php
	//$tehAbsoluteURL = "http://localhost/dentistnyc2/";
	$pageTitle = "Dr. Inna Chern: Schedule an Appointment - Contact Us";
	$pageKeywords = "";
	$pageDesc = "";
	require_once("tehPHP/dentHeader.php");
?>
<style>
	.contentSubTitle
</style>
<div class="dentistBG whiteText">
	<div class="centerWrap whiteBG stdBoxShadow contentShell" style="min-height: 400px;">	
		<div class="mainPageDirectionsShell">
			<div class="centerWrap">
				<div class="contentTitle">
					Contact One of Two Convenient locations
				</div>
				<div class="contentSubTitle">
					Brightening the Smiles of Manhattan and Brooklyn
				</div>
				<br /><br />
				<div class="gridShell">
					<div class="gridColumnShell">
						<br /><br /><br />
						<div class="contentSubTitle">
							Manhattan Office:
						</div>
				
						<div>
							<i class="fas fa-phone"></i> (212) 838-0842
						</div>
						<div>
							<i class="fas fa-envelope"></i> 964dental@gmail.com
						</div>
						<div>
							<i class="fas fa-address-card"></i> 150 E 58th St, 8th Floor Annex, New York, NY 10155
						</div>
						<p style="font-size: 1em;">
							We are conveniently located 2 blocks from the 4/5/6/Q/N/R 59th street station and 6 blocks from the E/M/F 51st street station.
						</p>
					</div>
					<div class="gridColumnShell">
						<div class="mainPageMapShell stdBoxShadow">
							<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3022.041239984298!2d-73.97038944807439!3d40.76111767922527!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c258fab8c8c681%3A0xa999c0c3a2766bb6!2sSmiles+of+NYC+-+Michael+Kosdon+DDS!5e0!3m2!1sen!2sus!4v1526147455902" width="100%" height="500" frameborder="0" style="border:0" allowfullscreen></iframe>
						</div>
					</div>
				</div>
				<div class="gridShell">
					<div class="gridColumnShell">
						<div class="mainPageMapShell stdBoxShadow">
							<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3027.5188058300264!2d-73.95093124807629!3d40.64049277923839!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c25b4feed94ef3%3A0x1f438f114c584d6!2s1854+Nostrand+Ave%2C+Brooklyn%2C+NY+11226!5e0!3m2!1sen!2sus!4v1526147398872" width="100%" height="500" frameborder="0" style="border:0" allowfullscreen></iframe>
						</div>
					</div>
					<div class="gridColumnShell">
						<br /><br /><br />
						<div class="contentSubTitle">
							Brooklyn Office:
						</div>
						<div>
							<i class="fas fa-phone"></i>  (718) 282-4142
						</div>
						<div>
							<i class="fas fa-envelope"></i> icherndds@gmail.com<
						</div>
						<div>
							<i class="fas fa-address-card"></i> 1854 Nostrand Ave, Brooklyn, NY 11226
						</div>
						<p style="font-size: 1em;">
							We are located outside the Newkirk station on the 2/5 train and street parking is available daily.
						</p>
					</div>
				</div>
				<div>
					<br /><br /><br />
					<div class="saveTimeBookOnline centerText">
						Save time, Book Online!
					</div>
					<div class="dentActionButtonShell centerText">
						<a class="dentGreenButton" href="https://www.zocdoc.com/practice/dr-inna-chern-dds-11530">
							Book Online!
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>



<?php
	//if (substr_count(dirname($_SERVER['PHP_SELF']), '/') == "1")
	if( (substr_count(dirname($_SERVER['PHP_SELF']), '/') == "1") || (dirname($_SERVER['PHP_SELF']) == "/"))
	{
		require_once("./tehPHP/dentFooter.php");
	}
	else
	{
		require_once("../tehPHP/dentFooter.php");
	}
?>