<?php
	$pageTitle = "Dr. Inna Chern: Dental Implant Bridges";
	$pageKeywords = "manhattan dental implant bridge, manhattan dental implant bridges, manhattan implant bridge, manhattan implant bridges, manhattan dental implants bridge,NY dental implant bridge, NY dental implant bridges, NY implant bridge, NY implant bridges, NY dental implants bridge,new york dental implant bridge, new york dental implant bridges, new york implant bridge, new york implant bridges, new york dental implants bridge,new york city dental implant bridge, new york city dental implant bridges, new york city implant bridge, new york city implant bridges, new york city dental implants bridge";
	$pageDesc = "Dr. Inna Chern discusses dental Implant Bridges.";
	require_once("../tehPHP/dentHeader.php");
?>
<div class="dentistBG whiteText">
	<div class="centerWrap whiteBG stdBoxShadow contentShell" style="min-height: 400px;">	
		<div class="mainPageDirectionsShell">
			<div class="centerWrap">
				<div class="contentTitle">
					Manhattan Dental Implant Bridges
				</div>
				<p class="darkGrayText italic contentQuote">
					Sometimes there are several implants in a row to replace several missing teeth and we use bridges to fix large areas in the mouth. Now that your implants are ready...we are ready to Load!
				</p>
				<div class="contentSubTitle">
					What to Expect: Three appointments
				</div>
				<p class="grayText">
					Dr. Chern will unscrew the healing caps and screw in  impression posts, which are specifically made for your implants size and shape. An x-ray is taken to ensure that the posts are fully engaged. An impression is then taken of the implants and posts to be sent to the lab for fabrication of the connected crowns or bridges. The posts are removed with the impression and the healing caps are replaced. In two weeks, we will see you for the insertion of the bridge. Depending on the angulation, screw retained or cementable bridges will be fabricated.
				</p>
				<p class="grayText">
					The healing cap is removed and the custom abutments and bridge are placed in the mouth for tryin. An xray is taken to ensure proper engagement, Dr. Chern goes on to check the gums around the bridge and the bite. Once the fit is confirmed, Dr. Chern discusses the feel and aesthetics with you. The implants are tightened and post insertion instructions are given to the patient. 
				</p>
				<p class="grayText">
					The final appointment is scheduled one week later. This is a quick and easy appointment to evaluate occlusion and take impressions for a nightguard. The guard acts to protect the implant from excess forces.
				</p>
			</div>
		</div>
	</div>
</div>

<?php
	require_once("../tehPHP/dentFooter.php");
?>