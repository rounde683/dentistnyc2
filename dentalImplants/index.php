<?php
	$pageTitle = "Dr. Inna Chern: Dental Implant Dentistry - Dental Implant Options";
	$pageKeywords = "";
	$pageDesc = "Dr. Inna Chern discusses who should consider Dental Implants what to expect for dental implant procedures.";
	require_once("../tehPHP/dentHeader.php");
?>
<div class="dentistBG whiteText">
	<div class="centerWrap whiteBG stdBoxShadow contentShell" style="min-height: 400px;">	
		<div class="mainPageDirectionsShell">
			<div class="centerWrap">
				<div class="contentTitle">
					DENTAL IMPLANT Services and Options
				</div>
			</div>
		</div>
	</div>
</div>



<?php
	require_once("../tehPHP/dentFooter.php");
?>