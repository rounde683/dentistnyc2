<?php
	$pageTitle = "Dr. Inna Chern: Dental Implant Dentistry - Dental Implants";
	$pageKeywords = "manhattan dental implants, manhattan implant dentist, manhattan implant dentistry,NY dental implants, NY implant dentist, NY implant dentistry,new york dental implants, new york implant dentist, new york implant dentistry,new york city dental implants, new york city implant dentist, new york city implant dentistry";
	$pageDesc = "Dr. Inna Chern discusses who should consider Dental Implants what to expect for dental implant procedures.";
	require_once("../tehPHP/dentHeader.php");
?>
<div class="dentistBG whiteText">
	<div class="centerWrap whiteBG stdBoxShadow contentShell" style="min-height: 400px;">	
		<div class="mainPageDirectionsShell">
			<div class="centerWrap">
				<div class="contentTitle">
					MANHATTAN DENTAL IMPLANTS
				</div>
				<p class="grayText">
					If you are missing or lost a tooth and eating has become uncomfortable and your teeth are shifting, we have the answer for you. Dental implants are the best alternative to replace missing teeth!
				</p>
				<div class="contentSubTitle">
					YOU ARE A CANDIDATE FOR DENTAL IMPLANTS IF YOU:
				</div>
				<ul class="contentList grayText">
					<li>are in good health</li>
					<li>have a good amount of bone in the area of the missing tooth</li>
					<li>have good oral hygiene</li>
				</ul>
				<p class="grayText">
					Dental implants are metal  or ceramic posts that are surgically positioned into the jawbone beneath your gums to provide stable support for artificial teeth or prosthetics. Some people who have lost bone in their jaw can still get implants, but the bone must be replaced with graft material. The post is allowed to integrate into your bone for 3-6 months.
				</p>
				<div class="contentTitle">
					WHAT TO EXPECT
				</div>
				<p class="grayText">
					•	The area is given local anesthesia and a surgical stent is placed. The stent allows for precision placement of the implant for ideal crown placement after healing. The implant is inserted into the bone. The gums will be sewn above the implant site and you will be left to heal for about three to six months depending on the quantity and quality of the bone in the area. After a  three month follow-up is scheduled to evaluate the area and to check if the implant has integrated into the bone properly. Once the osseointegration has been comfirmed, the implant is ready for its prosthetic or crown.
				</p>
			</div>
		</div>
	</div>
</div>



<?php
	require_once("../tehPHP/dentFooter.php");
?>