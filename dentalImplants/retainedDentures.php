<?php
	$pageTitle = "Dr. Inna Chern: Dental Implants Retained Dentures";
	$pageKeywords = "manhattan dental implants dentures, manhattan dental implant dentures, manhattan implant dentures, manhattan retained dentures, manhattan dental implant retained dentures,new york city dental implants dentures, new york city dental implant dentures, new york city implant dentures, new york city retained dentures, new york city dental implant retained dentures,new york dental implants dentures, new york dental implant dentures, new york implant dentures, new york retained dentures, new york dental implant retained dentures,NY dental implants dentures, NY dental implant dentures, NY implant dentures, NY retained dentures, NY dental implant retained dentures";
	$pageDesc = "Dr. Inna Chern discusses dental implants and retained dentures.";
	require_once("../tehPHP/dentHeader.php");
?>
<div class="dentistBG whiteText">
	<div class="centerWrap whiteBG stdBoxShadow contentShell" style="min-height: 400px;">	
		<div class="mainPageDirectionsShell">
			<div class="centerWrap">
				<div class="contentTitle">
					Manhattan Dental Implants Retained Dentures
				</div>
				<div class="contentSubTitle">
					Brightening the Smiles of Manhattan and Brooklyn
				</div>
				<p class="darkGrayText italic contentQuote">
					Implants Can Do a World of Good in Helping Stabilize Dentures. If you have Loose or moving dentures, there are a slew of options that implants can offer you to help stabilize your teeth!
				</p>
				<p class="grayText">
					Because every case is unique and different depending on the amount of missing teeth and periodontal condition, its best to schedule an appointment for a consultation with Dr. Chern in either the Manhattan or Brooklyn location.
				</p>
			</div>
		</div>
	</div>
</div>

<?php
	require_once("../tehPHP/dentFooter.php");
?>