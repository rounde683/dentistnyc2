<?php
	$pageTitle = "Dr. Inna Chern: Dental Implant Crowns";
	$pageKeywords = "manhattan dental implant crowns, manhattan dentist implant crowns, manhattan implant crowns, manhattan crowns implant, manhattan dental implant crown, manhattan dentist implant crown, manhattan implant crown, manhattan crown implant,new york dental implant crown, new york dentist implant crown, new york implant crown, new york crown implant, NY dental implant crown, NY dentist implant crown, NY implant crown, NY crown implant, new york city dental implant crown, new york city dentist implant crown, new york city implant crown, new york city crown implant, manhattan";
	$pageDesc = "Dr. Inna Chern discusses dental Implant Crowns and what to expect if you're looking to have the procedure.";
	require_once("../tehPHP/dentHeader.php");
?>
<div class="dentistBG whiteText">
	<div class="centerWrap whiteBG stdBoxShadow contentShell" style="min-height: 400px;">	
		<div class="mainPageDirectionsShell">
			<div class="centerWrap">
				<div class="contentTitle">
					Manhattan Dental Implant Crowns
				</div>
				<div class="contentSubTitle">
					What to Expect: Three appointments
				</div>
				<p class="darkGrayText italic contentQuote">
					"Now that your implant is ready...we are ready to Load!"
				</div>
				<p class="grayText">
					Dr. Chern will unscrew the healing cap and screw in an impression post, which is specifically made for your implants size and shape. An x-ray is taken to ensure that the post is fully engaged. An impression is then taken of the implant and post to be sent to the lab for fabrication of the crown. The post is removed with the impression and the healing cap is replaced. In two weeks, we will see you for the insertion of the crown. Depending on the angulation, a screw retained or cementable crown will be fabricated.
				</p>
				<p class="grayText">
					The healing cap is removed and the custom abutment and crown are placed in the mouth for tryin. An xray is taken to ensure proper engagement, Dr. Chern goes on to check the gums around the crown and the bite. Once the fit is confirmed, Dr. Chern discusses the feel and aesthetics with you. The implant is tightened and post insertion instructions are given to the patient. 
				<p class="grayText">
					The final appointment is scheduled one week later. This is a quick and easy appointment to evaluate occlusion and take impressions for a nightguard. The guard acts to protect the implant from excess forces.
				</p>
			</div>
		</div>
	</div>
</div>
<?php
	require_once("../tehPHP/dentFooter.php");
?>