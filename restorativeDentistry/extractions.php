<?php
	$pageTitle = "Dr. Inna Chern: Restorative Dentistry - Extractions";
	$pageKeywords = "manhattan tooth extractions, manhattan dental extractions, manhattan tooth extraction, manhattan tooth extraction procedure, manhattan tooth extractions, manhattan dentist extractions, manhattan dentists extractions, NY tooth extractions, NY dental extractions, NY tooth extraction, NY tooth extraction procedure, NY dentist extractions, NY dentists extractions, NYC tooth extractions, NYC dental extractions, NYC tooth extraction, NYC tooth extraction procedure, NYC dentist extractions, NYC dentists extractions, new york tooth extractions, new york dental extractions, new york tooth extraction, new york tooth extraction procedure, new york dentist extractions, new york dentists extractions, new york city tooth extractions, new york city dental extractions, new york city tooth extraction, new york city tooth extraction procedure, new york city dentist extractions, new york city dentists extractions";
	$pageDesc = "Restorative extractions services of Manhattan. Dr. Inna Chern discusses some of the expectations of the procedure and clues as to when the procedure is necessary.";
	require_once("../tehPHP/dentHeader.php");
?>
<div class="dentistBG whiteText">
	<div class="centerWrap whiteBG stdBoxShadow contentShell" style="min-height: 400px;">	
		<div class="mainPageDirectionsShell">
			<div class="centerWrap">
				<div class="contentTitle">
					Manhattan Tooth Extractions
				</div>
				<p class="darkGrayText italic contentQuote">
					"Extractions are never a procedure that Dr. Chern enjoys recommending but are sometimes necessary. Whether its a  wisdom tooth or decayed tooth, we strive to make it as painless as possible!"
				</p>
				<p class="grayText">
					Teeth are removed for several reasons including dental decay, periodontal disease, orthodontic treatments or trauma. In this day and age, every tooth can be replaced and Dr. Chern will discuss all replacement options prior to removal.
				</p>
				<div class="contentSubTitle">
					WHAT TO EXPECT DURING AN EXTRACTION
				</div>
				<p class="grayText">
					The area will be numbed around the tooth. The gum tissue is moved to expose more tooth structure and instruments are used to push the tooth out of its socket. The only sensation is that of pressure. Dr. Chern will periodically check how you are doing and allow for breaks to rest the jaw until the tooth is removed. The area is cleaned and irrigated. Stitches are placed if the tissue needs to be approximated but most routine extractions close on their own.
				</p>
				<div class="contentSubTitle">
					POST-OPERATIVE INSTRUCTIONS
				</div>
				<p class="grayText">
					After the tooth extraction, it’s important for a blood clot to form. This process starts instantaneously. Dr. Chern will place guaze in the area. Biting pressure will help stop the bleeding. Bite on the gauze pad for 30 to 45 minutes immediately after the appointment. If the bleeding or oozing persists, place another gauze pad and bite firmly for another 30 minutes. You may have to do this several times to staunch the flow of blood. If there is continued bleeding, place a tea bag over the area and call Dr. Chern for further instructions.
				</p>
				<p class="grayText">
					Do not rinse vigorously, suck on straws, smoke, drink alcohol or chew on food next to the extraction site for 24-48 hours. Smoking cessation is recommended for a week. After 24-48 hrours, we recommend warm salt water rinses and regular brushing.
				</p>
				<p class="grayText">
					Heavy lifting, exercise and other strenuous activities are not recommended till the tenderness and pain have ceased (48-72 hours).
				</p>
				<p class="grayText">
					Dr. Chern schedules a one week follow-up appointment to evaluate the area and discuss replacement options.
				</p>
			</div>
		</div>
	</div>
</div>


<?php
	require_once("../tehPHP/dentFooter.php");
?>