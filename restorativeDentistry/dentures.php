<?php
	//$tehAbsoluteURL = "http://localhost/dentistnyc2/";
	$pageTitle = "Dr. Inna Chern: Restorative Dentistry - Extractions";
	$pageKeywords = "manhattan dentures, manhattan denture dentist, manhattan denture dentistry,NY dentures, NY denture dentist, NY denture dentistry,NYC dentures, NYC denture dentist, NYC denture dentistry,new york dentures, new york denture dentist, new york denture dentistry,new york city dentures, new york city denture dentist, new york city denture dentistry";
	$pageDesc = "Restorative extractions services of Manhattan. Dr. Inna Chern discusses some of the expectations of the procedure and clues as to when the procedure is necessary.";
	require_once("../tehPHP/dentHeader.php");
?>
<div class="dentistBG whiteText">
	<div class="centerWrap whiteBG stdBoxShadow contentShell" style="min-height: 400px;">	
		<div class="mainPageDirectionsShell">
			<div class="centerWrap">
				<div class="contentTitle">
					Manhattan Dentures
				</div>
				<div class="contentSubTitle">
					Brightening the Smiles of Manhattan and Brooklyn
				</div>
				<p class="grayText">
					Dentures are routinely made to replace missing teeth both as a temporary or permanent restoration. Dentures are a removable replacement for missing teeth. Existing teeth may need to be prepared for dentures to fit properly. Teeth may need to be extracted and your mouth given a chance to heal before new dentures can be placed.
				</p>
				<div class="contentSubTitle">
					IMMEDIATE DENTURES
				</div>
				<p class="grayText">
					Immediate dentures can be put in once the teeth are removed. Dentures may require adjustments once healing takes place, and achieving optimal comfort and fit could take several appointments. After proper fit has been achieved the dentures may take some getting used to, but over time a patient’s smile is enhanced with this easy-to-care-for solution. After 12-16 months the denture may start to loosen and a final set or a reline may be necessary.
				</p>
				<div class="contentSubTitle">
					HOW TO CARE FOR DENTURES
				</div>
				<p class="grayText">
					After you receive your dentures, you’ll be instructed on how to properly clean them to eliminate bacteria and other buildup. Make sure to handle your dentures carefully, and store them in a safe area away from falls and pets.  It is important to clean your mouth and brush your dentures daily.
				</p>
				<div class="contentSubTitle">
					SCHEDULE REGULAR CHECK UPS
				</div>
				<p class="grayText">
					Dental abscess' are a dental emergency and should be treated immediately. Dental swellings can spread and become dangerous especially when accompanied by difficulty swallowing and/or breathing. If breathing difficulties occur, it is recommended to goto a nearby emergency room or call 911.
				</p>
				<p class="grayText">
					Regular dental check ups are still necessary to have your dentures examined and professionally cleaned. Dr. Chern will check the soft tissues and fit of the denture every 6-12 months. Seeing the dentist maintains the health of all aspects of the mouth with or without teeth.
				</p>
				<p class="grayText">
					Contact our Manhattan or Brooklyn office to discuss your tooth replacement options today!
				</p>
			</div>
		</div>
	</div>
</div>


<?php
	require_once("../tehPHP/dentFooter.php");
?>