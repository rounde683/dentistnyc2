<?php
	$pageTitle = "Dr. Inna Chern: Restorative Dentistry - Crowns &amp; Bridges";
	$pageKeywords = "manhattan dental crowns and bridges, manhattan dental crowns & bridges, manhattan tooth crowns and bridges, manhattan tooth crowns & bridges,NY dental crowns and bridges, NY dental crowns & bridges, NY tooth crowns and bridges, NY tooth crowns & bridges,NYC dental crowns and bridges, NYC dental crowns & bridges, NYC tooth crowns and bridges, NYC tooth crowns & bridges,new york dental crowns and bridges, new york dental crowns & bridges, new york tooth crowns and bridges, new york tooth crowns & bridges,new york city dental crowns and bridges, new york city dental crowns & bridges, new york city tooth crowns and bridges, new york city tooth crowns & bridges";
	$pageDesc = "Restorative Crowns and Bridges services of Manhattan. Dr. Inna Chern discusses the pros and cons of Crowns and Bridges.";
	require_once("../tehPHP/dentHeader.php");
?>
<div class="dentistBG whiteText">
	<div class="centerWrap whiteBG stdBoxShadow contentShell" style="min-height: 400px;">	
		<div class="mainPageDirectionsShell">
			<div class="centerWrap">
				<div class="contentTitle">
					Manhattan Crowns &amp; Bridges
				</div>
				<p class="darkGrayText italic contentQuote">
					"Did you just have a root canal or a large filling that broke? Or an area that has a large food trap around it. The best coarse of treatment may be to consider a crown."
				</p>
				<div class="contentSubTitle">
					WHAT ARE PORCELAIN CROWNS?
				</div>
				<p class="grayText">
					In this day and age, advances in the strength and appearance of porcelain/ceramic crowns have made them the leading choice in restoring damaged tooth structure. While we eat, we exert over 300 pounds of force on our teeth. A  damaged tooth due to root canal, grinding, decay or fracture, is likely to break from the stress of daily use. Since the crown covers the entire top portion of your tooth, it provides ideal strength so you can resume normal function.
				</p>
				<div class="contentSubTitle">
					WHAT TO EXPECT DURING A CROWN PROCEDURE
				</div>
				<div class="grayText">
					FIRST VISIT (APPROXIMATELY 1 HOUR)
				</div>
				<p class="grayText">
					Dr. Chern will numb the area with  local anesthesia around the area of the tooth being treated. The tooth will then be prepared in every dimension  (1.5 to 2mm) to create room for the porcelain crown. Dr. Chern will take impressions of the area so that the lab can fabricate 3D models. These models will be scanned and used to create the ceramic crown.
				</p>
				<p class="grayText">
					A temporary crown is created and cemented with temporary cement so that the remaining tooth structure remains protected. For the subsequent 1-2 weeks, we recommend a softer diet and avoiding vigorous flossing around the area. If the temporary crown falls out, it is important to try to replace it with OTC temporary cement and call either the Brooklyn or Manhattan office ASAP.
				</p>
				<div class="grayText">
					SECOND VISIT
				</div>
				<p class="grayText">
					Dr. Chern will numb the area with  local anesthesia around the area of the tooth being treated. The tooth will then be prepared in every dimension  (1.5 to 2mm) to create room for the porcelain crown. Dr. Chern will take impressions of the area so that the lab can fabricate 3D models. These models will be scanned and used to create the ceramic crown.
				</p>
				<div class="contentSubTitle">
					PORCELAIN BRIDGES
				</div>
				<p class="darkGrayText italic contentQuote">
					Things can happen, leaving a tooth to need to be removed. In some instances, Implants are not an option and that's where porcelain/ceramic bridges come into the picture as a great treatment to replace a tooth. 
				</p>
				<p class="grayText">
					A bridge requires a few appointments but can also be a faster resolution of an area versus an implant. Dr. Chern will discuss all options with you during the consultation. With proper care, a bridge can last for years.
				</p>
				<div class="contentSubTitle">
					WHAT TO EXPECT DURING A BRIDGE PROCEDURE
				</div>
				<div class="contentSubTitleCaption grayText">
					2-3 Visits
				</div>
				<div class="grayText">
					FIRST VISIT
				</div>
				<p class="grayText">
					Dr. Chern will numb the area around the teeth on each side of the missing tooth and they will be prepared for crowns. Careful attention will be placed on preparing the teeth with identical line angles to help the dental ceramist create a bridge that has an easy path of insertion. An impression will be taken of the teeth and a temporary bridge will be fabricated and cemented with temporary cement. Your next appointment will be in one to two weeks.
				</p>
				<div class="grayText">
					SECOND VISIT
				</div>
				<p class="grayText">
					A framework or the final bridge will be tried in. Dr. Chern will check the margins and bite. An x-ray will be taken to ensure fit. For shorter spans or simpler case, the bridge will be cemented with temporary cement so the patient can try out the prosthesis for comfort and color. If a framework was used, then a new impression is taken which allows the lab to apply the final layer of ceramic more accurately.
				</p>
				<div class="grayText">
					THIRD VISIT
				</div>
				<p class="grayText">
					If the patient was comfortable during the trial period, the final bridge will be permanently cemented. If the framework was used, the  final bridge will be tried in and assessed for function, comfort and aesthetic approval. The bridge is then cemented with permanent cement. Post cementation instructions are given and the area is cleaned and flossed. Floss threaders are recommended for cleaning in the area.
				</p>
			</div>
		</div>
	</div>
</div>



<?php
	require_once("../tehPHP/dentFooter.php");
?>