<?php
	//$tehAbsoluteURL = "http://localhost/dentistnyc2/";
	$pageTitle = "Dr. Inna Chern: Manhattan Tooth Colored Filings";
	$pageKeywords = "manhattan tooth colored fillings, manhattan tooth colored filling, manhattan white colored filling, manhattan white colored fillings, manhattan tooth-colored fillings, manhattan tooth-colored filling,new york tooth colored fillings, new york tooth colored filling, new york white colored filling, new york white colored fillings, new york tooth-colored fillings, new york tooth-colored filling,new york city tooth colored fillings, new york city tooth colored filling, new york city white colored filling, new york city white colored fillings, new york city tooth-colored fillings, new york city tooth-colored filling,NY tooth colored fillings, NY tooth colored filling, NY white colored filling, NY white colored fillings, NY tooth-colored fillings, NY tooth-colored filling";
	$pageDesc = "Manhattan Cosmetic Dental Services. Learn about our Tooth Colored Fillings and schedule an appointment at our manhattan offices now.";
	require_once("../tehPHP/dentHeader.php");
?>

<div class="dentistBG whiteText">
	<div class="centerWrap whiteBG stdBoxShadow contentShell" style="min-height: 400px;">	
		<div class="mainPageDirectionsShell">
			<div class="centerWrap">
				<div class="contentTitle">
					Manhattan Tooth Colored Fillings
				</div>
				<p class="darkGrayText italic contentQuote">
					Dr. Chern believes in the mercury-free approach to tooth restoration. Kiss those old silver fillings goodbye!
				</div>
				<p class="grayText">
					When a cavity is diagnosed through clinical exam and radiology, Dr. Chern  removes the areas of decay in the tooth and fills the voids with a durable dental composite or ceramic material. These materials are durable, long-lasting and highly aesthetic. During your initial appointment, Dr. Chern can discuss your options to find an ideal fit for your dental needs.
				</p>
			</div>
		</div>
	</div>
</div>


<?php
	require_once("../tehPHP/dentFooter.php");
?>