<?php
	//$tehAbsoluteURL = "http://localhost/dentistnyc2/";
	$pageTitle = "Dr. Inna Chern: Manhattan Restorative Dental Services &amp; Brooklyn Restorative Dental Services";
	$pageKeywords = "";
	$pageDesc = "Manhattan Restorative Dental Services. Learn about our Restorative Dental services and schedule an appointment now.";
	require_once("../tehPHP/dentHeader.php");
?>

<div class="dentistBG whiteText">
	<div class="centerWrap whiteBG stdBoxShadow contentShell" style="min-height: 400px;">	
		<div class="mainPageDirectionsShell">
			<div class="centerWrap">
				<div class="contentTitle">
					Restorative Dental Services
				</div>
				<div class="contentSubTitle">
					Brightening the Smiles of Manhattan and Brooklyn
				</div>
			</div>
		</div>
	</div>
</div>


<?php
	require_once("../tehPHP/dentFooter.php");
?>