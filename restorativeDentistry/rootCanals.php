<?php
	$pageTitle = "Dr. Inna Chern: Restorative Dentistry - Extractions";
	$pageKeywords = "manhattan tooth root canal, manhattan tooth root canals, manhattan dental root canals, manhattan dental root canal, manhattan root canal, manhattan root canals, manhattan root canal dentist, manhattan root canal dentistry,NY tooth root canal, NY tooth root canals, NY dental root canals, NY dental root canal, NY root canal, NY root canals, NY root canal dentist, NY root canal dentistry,NYC tooth root canal, NYC tooth root canals, NYC dental root canals, NYC dental root canal, NYC root canal, NYC root canals, NYC root canal dentist, NYC root canal dentistry,new york tooth root canal, new york tooth root canals, new york dental root canals, new york dental root canal, new york root canal, new york root canals, new york root canal dentist, new york root canal dentistry,new york city tooth root canal, new york city tooth root canals, new york city dental root canals, new york city dental root canal, new york city root canal, new york city root canals, new york city root canal dentist, new york city root canal dentistry";
	$pageDesc = "Restorative extractions services of Manhattan. Dr. Inna Chern discusses some of the expectations of the procedure and clues as to when the procedure is necessary.";
	require_once("../tehPHP/dentHeader.php");
?>
<div class="dentistBG whiteText">
	<div class="centerWrap whiteBG stdBoxShadow contentShell" style="min-height: 400px;">	
		<div class="mainPageDirectionsShell">
			<div class="centerWrap">
				<div class="contentTitle">
					Manhattan Dental Root Canals
				</div>
				<p class="darkGrayText italic contentQuote">
					"With the advent of new technology, root canals are a breeze! Gone are the days when this treatment earned its bad reputation!"
				</p>
				<p class="grayText">
					Root canal therapy can save a tooth that has become severely infected or decayed. Schedule an appointment at either the Manhattan or Brooklyn office, if you are experiencing any dental pain. First and foremost, Dr. Chern will provide you with several pain management options.
				</p>
				<div class="contentSubTitle">
					SIGNS AND SYMPTOMS YOU NEED A ROOT CANAL
				</div>
				<ul class="contentList grayText">
					<li>Spontaneous pain even when you’re not eating or drinking</li>
					<li>Pain that wakes you up in the middle of the night</li>
					<li>Toothache that worsens when pressure is applied or when eating</li>
					<li>Darkening or discoloration of a tooth</li>
					<li>Recurring swelling of the gums</li>
					<li>Extreme sensitivity to cold or heat</li>
					<li>swelling of the gums or cheek</li>
				</ul>
				<div class="contentSubTitle">
					DURING YOUR ROOT CANAL
				</div>
				<p class="grayText">
					Local anesthesia is used to get the area numb and the pulp of the tooth is cleaned out of infected tissue. If your tooth was severely infected, we may opt to place a medication in your canals for one or two weeks before completing the root canal to effectively decontaminate the area. A temporary filling is placed in between treatments to keep the area sterile. The height of the tooth is reduced to minimize trauma to the tooth while its healing.
				</p>
				<div class="contentSubTitle">
					AFTER YOUR ROOT CANAL
				</div>
				<p class="grayText">
					You should avoid chewing on that side of your mouth for a couple of days or until the tenderness has subsided. After the root canal is complete, the tooth will be brittle and require more strength from a crown.
				</p>
			</div>
		</div>
	</div>
</div>



<?php
	require_once("../tehPHP/dentFooter.php");
?>