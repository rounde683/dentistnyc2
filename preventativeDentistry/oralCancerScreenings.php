<?php
	//$tehAbsoluteURL = "http://localhost/dentistnyc2/";
	$pageTitle = "Dr. Inna Chern: Manhattan Oral Cancer Screening &amp; Brooklyn Oral Cancer Screening";
	$pageKeywords = "manhattan oral cancer screening, manhattan dental cancer screening, manhattan oral cancer screening, manhattan dentist cancer screening, manhattan oral cancer screening, manhattan dentists cancer screening, manhattan oral cancer screenings, manhattan dental cancer screenings, manhattan oral cancer screenings, manhattan dentist cancer screenings, manhattan oral cancer screenings, manhattan dentists cancer screenings, NY oral cancer screening, NY dental cancer screening, NY oral cancer screening, NY dentist cancer screening, NY oral cancer screening, NY dentists cancer screening, NY oral cancer screenings, NY dental cancer screenings, NY oral cancer screenings, NY dentist cancer screenings, NY oral cancer screenings, NY dentists cancer screenings, NYC oral cancer screening, NYC dental cancer screening, NYC oral cancer screening, NYC dentist cancer screening, NYC oral cancer screening, NYC dentists cancer screening, NYC oral cancer screenings, NYC dental cancer screenings, NYC oral cancer screenings, NYC dentist cancer screenings, NYC oral cancer screenings, NYC dentists cancer screenings, new york oral cancer screening, new york dental cancer screening, new york oral cancer screening, new york dentist cancer screening, new york oral cancer screening, new york dentists cancer screening, new york oral cancer screenings, new york dental cancer screenings, new york oral cancer screenings, new york dentist cancer screenings, new york oral cancer screenings, new york dentists cancer screenings, new york city oral cancer screening, new york city dental cancer screening, new york city oral cancer screening, new york city dentist cancer screening, new york city oral cancer screening, new york city dentists cancer screening, new york city oral cancer screenings, new york city dental cancer screenings, new york city oral cancer screenings, new york city dentist cancer screenings, new york city oral cancer screenings, new york city dentists cancer screenings";
	$pageDesc = "Manhattan Dental Services ranging from preventative care to dental implants and cosmetic.";
	require_once("../tehPHP/dentHeader.php");
?>
<div class="dentistBG whiteText">
	<div class="centerWrap whiteBG stdBoxShadow contentShell" style="min-height: 400px;">	
		<div class="mainPageDirectionsShell">
			<div class="centerWrap">
				<div class="contentTitle">
					Manhattan Oral Cancer Screening
				</div>
				<p class="grayText">
					Oral cancer is the sixth most common cancer with a high mortality rate.  Anyone can get oral cancer but it is most common in drinkers and smokers. We want to ensure you understand the importance of oral cancer screening because early detection will greatly improve the outcome if diagnosed.
				</p>
				<div class="contentSubTitle">
					Early Detection to Save
				</div>
				<p class="grayText">
					First and foremost, it is important to visit your dentist regularly for checkups. During a routine exam, Dr. Chern will check for oral cancer signs such as irregularity of the tissues and any bumps in your mouth, neck, and head. If she notices anything unusual or concerning she may refer you for further treatment with an Oral surgeon for a biopsy.
				</p>
				<div class="contentSubTitle">
					Signs of Oral Cancer
				</div>
				<ul class="contentList grayText">
					<li>Red or white sores/spots in the mouth or on the sides of the tongue.</li>
					<li>A sore that does not heal within 1-2 weeks or bleeds easily.</li>
					<li>Pain, tenderness, or numbness in the mouth or on the lips.</li>
					<li>Spots or freckles in the mouth.</li>
					<li>Problems chewing, swallowing, speaking or moving your mouth.</li>
				</ul>
				<div class="contentSubTitle">
					Prevention of Oral Cancer
				</div>
				<p class="grayText">
					The number one way to prevent oral cancer is to avoid all tobacco products and excessive alcohol use. Overall healthy lifestyle choices are also crucial to prevention and maintaining reduced inflammation in the body. Inflammation is the cause of most systemic diseases including cancer. Other tips include limiting sun exposure, and always using sunscreen especially around the face and lip area.
				</p>
				<div class="contentSubTitle">
					Come Visit us Today to Check for Possible Areas of Concern
				</div>
				<p class="grayText">
					Dr. Chern performs an oral cancer screening during all standard exams.
				</p>
			</div>
		</div>
	</div>
</div>



<?php
	require_once("../tehPHP/dentFooter.php");
?>