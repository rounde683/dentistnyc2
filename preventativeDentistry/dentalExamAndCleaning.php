<?php
	//$tehAbsoluteURL = "http://localhost/dentistnyc2/";
	$pageTitle = "Dr. Inna Chern: Manhattan Oral Cancer Screening &amp; Brooklyn Oral Cancer Screening";
	$pageKeywords = "manhattan dental exam and cleaning, manhattan dental exam & cleaning, manhattan dentist exam and cleaning, manhattan dentist exam & cleaning, manhattan dentists exam and cleaning, manhattan dentists exam & cleaning, manhattan dental exam and cleanings, manhattan dental exam & cleanings, manhattan dentist exam and cleanings, manhattan dentist exam & cleanings, manhattan dentists exam and cleanings, manhattan dentists exam & cleanings, NY dental exam and cleaning, NY dental exam & cleaning, NY dentist exam and cleaning, NY dentist exam & cleaning, NY dentists exam and cleaning, NY dentists exam & cleaning, NY dental exam and cleanings, NY dental exam & cleanings, NY dentist exam and cleanings, NY dentist exam & cleanings, NY dentists exam and cleanings, NY dentists exam & cleanings, NYC dental exam and cleaning, NYC dental exam & cleaning, NYC dentist exam and cleaning, NYC dentist exam & cleaning, NYC dentists exam and cleaning, NYC dentists exam & cleaning, NYC dental exam and cleanings, NYC dental exam & cleanings, NYC dentist exam and cleanings, NYC dentist exam & cleanings, NYC dentists exam and cleanings, NYC dentists exam & cleanings, new york dental exam and cleaning, new york dental exam & cleaning, new york dentist exam and cleaning, new york dentist exam & cleaning, new york dentists exam and cleaning, new york dentists exam & cleaning, new york dental exam and cleanings, new york dental exam & cleanings, new york dentist exam and cleanings, new york dentist exam & cleanings, new york dentists exam and cleanings, new york dentists exam & cleanings, new york city dental exam and cleaning, new york city dental exam & cleaning, new york city dentist exam and cleaning, new york city dentist exam & cleaning, new york city dentists exam and cleaning, new york city dentists exam & cleaning, new york city dental exam and cleanings, new york city dental exam & cleanings, new york city dentist exam and cleanings, new york city dentist exam & cleanings, new york city dentists exam and cleanings, new york city dentists exam & cleanings";
	$pageDesc = "Manhattan Dental Services ranging from preventative care to dental implants and cosmetic.";
	require_once("../tehPHP/dentHeader.php");
?>
<div class="dentistBG whiteText">
	<div class="centerWrap whiteBG stdBoxShadow contentShell" style="min-height: 400px;">	
		<div class="mainPageDirectionsShell">
			<div class="centerWrap">
				<div class="contentTitle">
					Manhattan Dental Exam and Cleaning
				</div>
				<p class="darkGrayText italic contentQuote">
					"Teeth contribute to our smiles and allow us to eat and speak properly. Taking care of them is important for our health and happiness. A smile is a dentists' window to the soul and allows us to communicate countless emotions."
				</p>
				<p class="grayText">
					Plaque and tarter accumulation around the teeth and under the gum-line can cause a myriad of dental problems such as gingivitis, bone loss, bad breath and tooth decay. Routine exams and cleanings aid in the removal of harmful bacteria and create a well balanced oral environment. Cleanings are recommended two to three times a year for most people and contribute to oral and systemic health along with giving us a pearly white smile. 
				</p>
				<div class="contentSubTitle">
					During your appointment: 
				</div>
				<p class="grayText">
					We will conduct a history of your dental health as well as any medical issues and concerns you may have about your smile. The examination includes low radiation digital X-rays, TMJ evaluation, oral cancer screening and manual examination of all the surfaces of your teeth and gums. The prophylaxis (cleaning) uses high powered ultra-sonic technology along with hand scaling to gently remove plaque and tarter deposits below and around the teeth. Prior to the end of your appointment, we will review oral hygiene tips, give you unique recommendations and answer any further questions you may have pertaining to your visit.
				</p>
			</div>
		</div>
	</div>
</div>



<?php
	require_once("../tehPHP/dentFooter.php");
?>