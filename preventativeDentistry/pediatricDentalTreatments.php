<?php
	//$tehAbsoluteURL = "http://localhost/dentistnyc2/";
	$pageTitle = "Dr. Inna Chern: Manhattan Pediatric Dental Treatments &amp; Brooklyn Pediatric Dental Treatments";
	$pageKeywords = "manhattan pediatric dental treatments, manhattan pediatric dentist, manhattan pediatric dentist treatments, manhattan pediatric dentist, manhattan pediatric dentists treatments, manhattan pediatric dentist, NY pediatric dental treatments, NY pediatric dentist, NY pediatric dentist treatments, NY pediatric dentist, NY pediatric dentists treatments, NY pediatric dentist, NYC pediatric dental treatments, NYC pediatric dentist, NYC pediatric dentist treatments, NYC pediatric dentist, NYC pediatric dentists treatments, NYC pediatric dentist, new york pediatric dental treatments, new york pediatric dentist, new york pediatric dentist treatments, new york pediatric dentist, new york pediatric dentists treatments, new york pediatric dentist, new york city pediatric dental treatments, new york city pediatric dentist, new york city pediatric dentist treatments, new york city pediatric dentist, new york city pediatric dentists treatments, new york city pediatric dentist";
	$pageDesc = "Manhattan Dental Services ranging from preventative care to dental implants and cosmetic.";
	require_once("../tehPHP/dentHeader.php");
?>
<div class="dentistBG whiteText">
	<div class="centerWrap whiteBG stdBoxShadow contentShell" style="min-height: 400px;">	
		<div class="mainPageDirectionsShell">
			<div class="centerWrap">
				<div class="contentTitle">
					Manhattan Pediatric Dental Treatmets
				</div>
				<p class="darkGrayText italic contentQuote">
					"Starting children off on a great dental note can set the tone for them enjoying their visits and maintaining a positive outlook on the dentist and their hygiene!""
				</p>
				<div class="contentSubTitle">
					Your Child's First Appointment
				</div>
				<p class="grayText">
					According to the American Academy of Pediatric Dentistry (AAPD), children should visit the dentists office around their first birthday.  Their new teeth begin to erupt between 6 months to age 3. It is important to evaluate the new teeth and review proper maintenance with the parents to ensure proper care. 
				</p>
				<p class="grayText">
					Dr. Chern sees children of all ages and initiates the appointment with a simple introduction to all her fun tools and dental toys. When the child is comfortable, she will initiate the intr-oral exam and count the teeth. A cleaning is done with the childs' choice of toothpaste and a Fluoride treatment is performed. Oral hygiene and nutritional instructions are reviewed with parents and children alike, to ensure families are on the same page in maintaining the childs' pearly whites.
				</p>
				<div class="contentSubTitle">
					Maintain Your Child's Healthy Smile
				</div>
				<p class="grayText">
					Our goal at Grand Street Dental is to establish a foundation of healthy oral habits so your child can have a lifetime of good oral health. We provide many preventative and restorative measures including teeth cleaning, fluoride treatments, nutrition recommendations, correcting improper bites, oral exams, and cavity risk assessments.
				</p>
				<p class="grayText">
					It is important to remember that the longer a dental visit is put off, the greater the risk of an undiagnosed or untreated oral complication becomes. Children are susceptible to cavities, gum disease, and other oral conditions, so it’s important to receive exams and treatments as early as possible. Dr. Plotnick’s preventative measures will address these issues and offer solutions to keep your child’s mouth healthy and pain-free!
				</p>
			</div>
		</div>
	</div>
</div>



<?php
	require_once("../tehPHP/dentFooter.php");
?>