<?php
	//$tehAbsoluteURL = "http://localhost/dentistnyc2/";
	$pageTitle = "Dr. Inna Chern: Manhattan Dental Services &amp; Brooklyn Dental Services";
	$pageKeywords = "manhattan emergency care dentist,manhattan dentist emergency care,manhattan emergency dentist services, manhattan emergency care dentist,manhattan dentist emergency care,manhattan emergency dentist services, manhattan emergency care dentist,manhattan dentist emergency care,manhattan emergency dentist services, NY emergency care dentist,NY dentist emergency care,NY emergency dentist services, NY emergency care dentist,NY dentist emergency care,NY emergency dentist services, NY emergency care dentist,NY dentist emergency care,NY emergency dentist services, NYC emergency care dentist, NYC dentist emergency care, NYC emergency dentist services, NYC emergency care dentist, NYC dentist emergency care, NYC emergency dentist services, NYC emergency care dentist, NYC dentist emergency care, NYC emergency dentist services, new york emergency care dentist, new york dentist emergency care, new york emergency dentist services, new york emergency care dentist, new york dentist emergency care, new york emergency dentist services, new york emergency care dentist, new york dentist emergency care, new york emergency dentist services, new york city emergency care dentist, new york city dentist emergency care, new york city emergency dentist services, new york city emergency care dentist, new york city dentist emergency care, new york city emergency dentist services, new york city emergency care dentist, new york city dentist emergency care, new york city emergency dentist services";
	$pageDesc = "Manhattan Dental Services ranging from preventative care to dental implants and cosmetic.";
	require_once("../tehPHP/dentHeader.php");
?>
<div class="dentistBG whiteText">
	<div class="centerWrap whiteBG stdBoxShadow contentShell" style="min-height: 400px;">	
		<div class="mainPageDirectionsShell">
			<div class="centerWrap">
				<div class="contentTitle">
					Manhattan Dental Emergency Services
				</div>
				<div class="contentSubTitle">
					Brightening the Smiles of Manhattan and Brooklyn
				</div>
				<p class="grayText">
					Dental emergencies happen at the most inopportune times and we strive to accomodate our patients as soon as possible to get them treated and out of pain. Call or email either our Manhattan or Brooklyn office at anytime and we will get you in to see Dr. Chern. 
				</p>
				<div class="contentSubTitle">
					In the Mean Time
				</div>
				<p class="grayText">
					Accidents happen and we are often left scurrying to find ways to alleviate the problem on our way to the office. Here are some helpful hints to manage the emergencies while you wait for care.
				</p>
				<div class="contentSubTitle">
					Bleeding Gums
				</div>
				<p class="grayText">
					If your gums are bleeding dispite regular cleanings this may be a dental emergency. Call either our Manhattan or Brooklyn office to get an appointment to have Dr. Chern evaluate the issue and schedule a checkup with your medical doctor to rule out systemic problems.
				</p>
				<div class="contentSubTitle">
					Dental Abscess and Swellings
				</div>
				<p class="grayText">
					Dental abscess' are a dental emergency and should be treated immediately. Dental swellings can spread and become dangerous especially when accompanied by difficulty swallowing and/or breathing. If breathing difficulties occur, it is recommended to goto a nearby emergency room or call 911.
				</p>
				<div class="contentSubTitle">
					Dental Trauma
				</div>
				<p class="grayText">
					If your permanent tooth falls out of your mouth due to injury, Dr. Chern recommends gently cleaning off the root from debris from the surface it fell on and replacing back in the area it fell from. If curcumstances do not permit this store the tooth in saliva or whole milk. The first hour is the most important to have a dentist replace the tooth.
				</p>
				<div class="contentSubTitle">
					As Soon as we See You
				</div>
				<p class="grayText">
					Dr. Chern will have you come to the office and evaluate the area. We will take necessary xrays and remove you from pain via local anesthesia. As soon as you are pain-free, Dr. Chern will advise you as to your treatment options and prescribe any necessary medications.
				</p>
			</div>
		</div>
	</div>
</div>



<?php
	require_once("../tehPHP/dentFooter.php");
?>