<?php
	$pageTitle = "Dr. Inna Chern: Manhattan Periodontal Treatments";
	$pageKeywords = "manhattan periodontal treatments, manhattan periodontal treatment, NY periodontal treatments, NY periodontal treatment, new york periodontal treatments, new york periodontal treatment, new york city periodontal treatments, new york city periodontal treatment";
	$pageDesc = "Manhattan Cosmetic Dental Services. Learn about our Periodontal Treatments and schedule an appointment now.";
	require_once("../tehPHP/dentHeader.php");
?>

<div class="dentistBG whiteText">
	<div class="centerWrap whiteBG stdBoxShadow contentShell" style="min-height: 400px;">	
		<div class="mainPageDirectionsShell">
			<div class="centerWrap">
				<div class="contentTitle">
					Manhattan Periodontal Treatments
				</div>
				<div class="contentSubTitle">
					DEEP CLEANINGS (SCALING & ROOT PLANING)
				</div>
				<p class="darkGrayText italic contentQuote">
					We have busy lives and busy schedules and a few missed appointments to the dentist for a cleaning can cause some major changes to your gums.  You may notice bleeding while brushing and flossing, and  you may even notice your teeth shifting. You may even notice a funny taste or smell from your mouth that is not very pleasant. This is your bodys' way of warning you its time to schedule that cleaning ASAP!
				</p>
				<p class="grayText">
					Some patients require more than routine preventative cleanings due to the excess accumulation of plaque and tarter under the gum line. Deep cleanings, can help alleviate many dental health problems by removing that hardened plaque from under the gum line so that the gums can heal from the inflammation caused by the plaque and tarter. Excess chronic inflammation can cause bone loss and eventual loss of teeth.
				</p>
				<div class="contentSubTitle">
					SIGNS TO LOOK FOR OF PERIODONTAL DISEASE:
				</div>
				<ul class="contentList grayText">
					<li>Bleeding when brushing and flossing</li>
					<li>Foul smell and chronic halitosis</li>
					<li>Red and inflamed gums that lack a healthy stippled appearance</li>
					<li>Large spaces and shifting of the teeth</li>
					<li>Deep pockets where food and bacteria get caught beneath your gums</li>
				</ul>
				<div class="contentSubTitle">
					WHAT TO EXPECT: 1-3 visits
				</div>
				<p class="grayText">
					During your first appointment, a full examination and routine dental cleaning is completed along with a full set of dental xrays and a full mouth measurement of the pockets around the teeth. Dr. Chern will discuss the need for deep cleanings in all or a few quadrants in the mouth. SHe will recommend either 1-2 follow-up appointments a few days after the gums heal from the routine cleaning. 
				</p>
				<p class="grayText">
					After full local anesthesia is achieved, a combination of ultrasonic instruments and hand scalers will be used to gently, yet efficiently rid your teeth of this harmful buildup. After each appointment, Dr. CHern will discuss hygiene tips and post care of the areas. She will recommend a prescription strength mouthwash for 3-5 days and/or warm saline rinses.
				</p>
				<p class="grayText">
					Your next  appointment will be in four months to evaluate the pocket depths, overall health of tissues and home care routine. Dr. Chern may recommend staying on a 4-6 month recall schedule for the duration of your visits.
				</p>
			</div>
		</div>
	</div>
</div>

<?php
	require_once("../tehPHP/dentFooter.php");
?>