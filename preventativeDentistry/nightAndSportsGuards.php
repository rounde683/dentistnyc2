<?php
	//$tehAbsoluteURL = "http://localhost/dentistnyc2/";
	$pageTitle = "Dr. Inna Chern: Manhattan Oral Cancer Screening &amp; Brooklyn Oral Cancer Screening";
	$pageKeywords = "manhattan night guard, manhattan sport guard, manhattan sports guard, manhattan night and sports guard, manhattan night & sports guards, manhattan dental night guard, manhattan dental sports guard, manhattan dental night and sports guard, manhattan night & sports guard, manhattan night guard, manhattan sport guard, manhattan sports guard, manhattan night and sports guard, manhattan night & sports guards, manhattan dentist night guard, manhattan dentist sports guard, manhattan dentist night and sports guard, manhattan night & sports guard, manhattan night guard, manhattan sport guard, manhattan sports guard, manhattan night and sports guard, manhattan night & sports guards, manhattan dentists night guard, manhattan dentists sports guard, manhattan dentists night and sports guard, manhattan night & sports guard, NY night guard, NY sport guard, NY sports guard, NY night and sports guard, NY night & sports guards, NY dental night guard, NY dental sports guard, NY dental night and sports guard, NY night & sports guard, NY night guard, NY sport guard, NY sports guard, NY night and sports guard, NY night & sports guards, NY dentist night guard, NY dentist sports guard, NY dentist night and sports guard, NY night & sports guard, NY night guard, NY sport guard, NY sports guard, NY night and sports guard, NY night & sports guards, NY dentists night guard, NY dentists sports guard, NY dentists night and sports guard, NY night & sports guard, NYC night guard, NYC sport guard, NYC sports guard, NYC night and sports guard, NYC night & sports guards, NYC dental night guard, NYC dental sports guard, NYC dental night and sports guard, NYC night & sports guard, NYC night guard, NYC sport guard, NYC sports guard, NYC night and sports guard, NYC night & sports guards, NYC dentist night guard, NYC dentist sports guard, NYC dentist night and sports guard, NYC night & sports guard, NYC night guard, NYC sport guard, NYC sports guard, NYC night and sports guard, NYC night & sports guards, NYC dentists night guard, NYC dentists sports guard, NYC dentists night and sports guard, NYC night & sports guard, new york night guard, new york sport guard, new york sports guard, new york night and sports guard, new york night & sports guards, new york dental night guard, new york dental sports guard, new york dental night and sports guard, new york night & sports guard, new york night guard, new york sport guard, new york sports guard, new york night and sports guard, new york night & sports guards, new york dentist night guard, new york dentist sports guard, new york dentist night and sports guard, new york night & sports guard, new york night guard, new york sport guard, new york sports guard, new york night and sports guard, new york night & sports guards, new york dentists night guard, new york dentists sports guard, new york dentists night and sports guard, new york night & sports guard, new york city night guard, new york city sport guard, new york city sports guard, new york city night and sports guard, new york city night & sports guards, new york city dental night guard, new york city dental sports guard, new york city dental night and sports guard, new york city night & sports guard, new york city night guard, new york city sport guard, new york city sports guard, new york city night and sports guard, new york city night & sports guards, new york city dentist night guard, new york city dentist sports guard, new york city dentist night and sports guard, new york city night & sports guard, new york city night guard, new york city sport guard, new york city sports guard, new york city night and sports guard, new york city night & sports guards, new york city dentists night guard, new york city dentists sports guard, new york city dentists night and sports guard, new york city night & sports guard";
	$pageDesc = "Manhattan Dental Services ranging from preventative care to dental implants and cosmetic.";
	require_once("../tehPHP/dentHeader.php");
?>
<div class="dentistBG whiteText">
	<div class="centerWrap whiteBG stdBoxShadow contentShell" style="min-height: 400px;">	
		<div class="mainPageDirectionsShell">
			<div class="centerWrap">
				<div class="contentTitle">
					Manhattan Dental Night &amp; Sports Guards
				</div>
				<p class="grayText">
					Mouth and Sport Guards are Utilized to help alleviate and signs and symptoms of grinding and clenching your teeth.
				</p>
				<div class="contentSubTitle">
					Grinding Your Teeth may Lead to the Following Issues:
				</div>
				<ul class="contentList grayText">
					<li>Extreme Jaw Pain (TMJ disorder)</li>
					<li>Abraded tooth surfaces</li>
					<li>Loss of vertical dimension of mouth</li>
					<li>Fractured/chipped Enamel</li>
					<li>Extreme Sensitivity to temperatures</li>
					<li>Headaches/ Migraines</li>
				</ul>
				<div class="contentSubTitle">
					The Benefits of Mouthguards and Nightguards
				</div>
				<p class="grayText">
					Grinding and clenching often occur during sleep and can negatively impact the quality of sleep you are experiencing every night. These appliances are often worn at night to prevent damage to the teeth and TMJ. They can also be worn during daytime if you find yourself clenching and grinding during the day. Sport guards are softer versions of mouth/night guards which are worn during vigorous athletic events or ones where traumatic injury is possible. Common sports include football. basketball and wrestling. 
				</p>
				<p class="grayText">
					Dr. Chern evaluates the TMJ and teeth for evidence of wear and chips. The guard is fabricated by taking impressions of the lower and upper jaw. A custom appliance is made in a laboratory which takes approximately one week. At the second appointment, Dr. Chern will tryin the appliance and check for an accurate fit. Post insertion instructions are given and we re-evaluate the appliance at every 6 month recall visit. We find our patients are very happy to report improved sleep and reduced tooth/jaw pain.
				</p>
			</div>
		</div>
	</div>
</div>



<?php
	require_once("../tehPHP/dentFooter.php");
?>