<?php
	//$tehAbsoluteURL = "http://localhost/dentistnyc2/";
	$pageTitle = "Dr. Inna Chern: About Dr. Inna Chern DDS";
	$pageKeywords = "";
	$pageDesc = "";
	require_once("tehPHP/dentHeader.php");
?>
<style>
	.contentSubTitle
</style>
<div class="dentistBG whiteText">
	<div class="centerWrap whiteBG stdBoxShadow contentShell" style="min-height: 400px;">	
		<div class="mainPageDirectionsShell">
			<div class="centerWrap">
				<div class="contentTitle centerText">
					Meet Dr. Inna Chern
				</div>
				<div class="contentSubTitle">
					
				</div>
				<br /><br />
				<div class="gridShell">
					<div class="gridColumnShell">
						<div style="padding: 50px 5%; text-align: center;">
							<img class="dentPortrait stdBoxShadowOnColorBG" width="300px" alt="Inna Chern DDS" src="<?php echo $tehAbsoluteURL; ?>layout/images/innaChernDDS-3.jpg" />
						</div>
					</div>
					<div class="gridColumnShell responsiveMargin">
						<p class="grayText">
							Dr Inna Chern- a native New Yorker, has been in private practice for more than fifteen years serving the greater New York area. Dr. Inna Chern attended New York University where she graduated with a Bachelor of Arts in Psychology in 1999. Pursuant of higher education, Dr. Chern continued her studies at Stony Brook University and received her Doctorate of Dental Medicine in 2003.
						</p>
						<p class="grayText">
							Returning to her New York City roots, she completed her General Practice Residency at Long Island College Hospital in 2004. She has split her time between private practice and teaching at Bronx Lebanon Dental Center since 2005. In her spare time, she enjoys spending time with her now 10 year of daughter who shares her love of running and yoga.
						</p>
					</div>
				</div>

				<div>
					<br /><br /><br />
					<div class="saveTimeBookOnline centerText">
						Schedule a Consultation
					</div>
					<div class="dentActionButtonShell centerText">
						<a class="dentGreenButton" href="https://www.zocdoc.com/practice/dr-inna-chern-dds-11530">
							Schedule Now!
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>



<?php
	//if (substr_count(dirname($_SERVER['PHP_SELF']), '/') == "1")
	if( (substr_count(dirname($_SERVER['PHP_SELF']), '/') == "1") || (dirname($_SERVER['PHP_SELF']) == "/"))
	{
		require_once("./tehPHP/dentFooter.php");
	}
	else
	{
		require_once("../tehPHP/dentFooter.php");
	}
?>