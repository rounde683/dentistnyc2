<?php
	//$tehAbsoluteURL = "http://localhost/dentistnyc2/";
	$pageTitle = "Dr. Inna Chern: Brightening the Smiles of NYC - Manhattan &amp; Brooklyn";
	$pageKeywords = "manhattan dentist, manhattan dental services, manhattan dental practice, , manhattan dentist practice, manhattan dental office, manhattan dentist practice, manhattan dental practice, manhattan dentist office, manhattan dentist practice, brooklyn dentist, brooklyn dental services, brooklyn dental practice, , brooklyn dentist practice, brooklyn dental office, brooklyn dentist practice, brooklyn dental practice, brooklyn dentist office, brooklyn dentist practice,NYC dentist, NYC dental services, NYC dental practice, , NYC dentist practice, NYC dental office, NYC dentist practice, NYC dental practice, NYC dentist office, NYC dentist practice,NY dentist, NY dental services, NY dental practice, , NY dentist practice, NY dental office, NY dentist practice, NY dental practice, NY dentist office, NY dentist practice,new york dentist, new york dental services, new york dental practice, , new york dentist practice, new york dental office, new york dentist practice, new york dental practice, new york dentist office, new york dentist practice,new york city dentist, new york city dental services, new york city dental practice, , new york city dentist practice, new york city dental office, new york city dentist practice, new york city dental practice, new york city dentist office, new york city dentist practice";
	$pageDesc = "Brightening the Smiles of NYC, one smile at a time. Dr. Inna Chern serving Manhattan &amp; Brooklyn";
	require_once("tehPHP/dentHeader.php");
?>

<div class="dentistSplashBG whiteText">
	<div class="centerWrap">
		<div class="gridShell">
			<div class="gridColumnShell">
				<div class="mainTitleSmall centerText" style="font-size: 4.5em; line-height: 1em; border: none; ">
					Brightening the Smiles of<br />NYC
				</div>
				<div class="mainTitleQuote centerText">
					"One Smile at a Time"
				</div>
				<div class="dentActionButtonShell">
					<a class="dentTransButton" href="#warmWelcome">
						Discover
					</a>
				</div>
			</div>
			<div class="gridColumnShell">
				<div class="whiteSplashShadowShell whiteBG stdBoxShadowOnColorBG">
					<div class="mainTitleSmall centerText blackText" style="font-weight: 600; border: none;">
						Plan Your Visit Now
					</div>
					<div class="dentActionButtonShell centerText">
						<a class="dentGreenButton" href="https://www.zocdoc.com/practice/dr-inna-chern-dds-11530">
							Book Now!
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<br style="clear: both;"/>
<div class="mainPageDirectionsShell" style="margin-bottom: -100px !important;">
	<div class="centerWrap">
		<div class="indexPagePaddingShell">
			<p class="grayText" style="font-size: 1.6em;">
				Dr. Inna Chern and her team aim to provide comprehensive care ranging from routine dental exams to cosmetic and implant dentistry.
			</p>
			<p class="grayText" style="font-size: 1.6em;"> We aim to treat each individuals' needs in a <b>relaxing</b> and <b>soothing environment</b> while creating <b>healthy</b> and <b>beautiful smiles</b>.
			</p>
		</div>
	</div>
</div>


<div class="revTechRevolutionSliderShell">
	<?php
		require_once("tehPHP/mainRevolutionSlider.php");
	?>
</div>

<div class="mainPageDirectionsShell" id="warmWelcome">
	<div class="centerWrap">
		<div class="gridShell">
			<div class="gridColumnShell">
				<div class="dentSquarePatternBG">
					<br />
					<center>
						<img class="whiteBG singlePhotoStyle mainPageBioPhoto" alt="Photo of Dr. Inna Chern DDS - Manhattan Dentist Services" title="Providing Dental Services to Manhattan &amp; Brooklyn" src="<?php echo $tehAbsoluteURL; ?>layout/images/innaChernDDS.jpg" />
					</center>
				</div>
			</div>
			<div class="gridColumnShell responsiveMargin">
				<div class="mainTitleSmall">
					Dr. Inna Chern
					<div class="leftTitleUnderline"></div>
				</div>
				<p class="grayText">
					As a Cosmetic and Implant Restorative Dentist, Dr. Inna Chern has been serving the New York City area for fifteen years and has an extensive following that includes people of all generations.  She prides herself in treating each patient as a family member of her practice. Everyone is greeted with a warm hello and a patient ear. She believes everyone is a unique individual with unique experiences and expectations. Her emphasis is to get to know each patient, their families and their individual needs in order to create a unique dental experience.
				</p>
				<p class="grayText">
					She takes pride in really getting to know all her patients, their families and their individual needs to create a unique dental experience.
				</p>
				<p class="grayText">
					"My love of seeing people smile is what bought me to choose dentistry as my lifelong profession. I was a dental assistant since the age of 15 and 	truly respected all my mentors. They infused me with their enthusiasm for the profession which propelled me to choose a similar path.
				</p>
				<p class="grayText">
					My goals for practicing dentistry are to educate people on the possibilities their smiles hold. I want to relay to people the importance of oral health and give them the information they need to make informed decisions on the value of a healthy and beautiful smile.
				</p>
				<p class="grayText">
					The most rewarding thing to me is when a patient tells me how happy and proud they are to smile. This can come from a simple cleaning or a cosmetic case coming to completion. It's an honor to help people in their journey to great oral and systemic health"
				</p>

				<div class="dentActionButtonShell centerText">
					<a class="dentGreenButton" href="<?php echo $tehAbsoluteURL; ?>aboutUs.php">
						Meet Dr. Inna Chern DDS
					</a>
				</div>
			</div>
		</div>
	</div>
</div>



<div class="mainPageDirectionsShell">
	<div class="centerWrap">
		<div class="gridShell">
			<div class="gridColumnShell">
				<div class="mainTitleSmall">
					Our Cases &amp; Patient Stories
				</div>
				<p>
					Don't take our word for it, Take a look at what our clients say about us.
				</p>
			</div>
			<div class="gridColumnShell">
				<div class="dentSquarePatternBG">
					<div class="gridShellThirtyThree">
						<div class="gridColumnShell patientShell">
							<div class="">
								<img width="100%" src="<?php echo $tehAbsoluteURL; ?>layout/images/patientOne.jpg" />
							</div>
						</div>
						<div class="gridColumnShell patientShell">
							<div class="">
								<img width="100%" src="<?php echo $tehAbsoluteURL; ?>layout/images/patientTwo.jpg" />
							</div>
						</div>
						<div class="gridColumnShell patientShell">
							<div class="">
								<img width="100%" src="<?php echo $tehAbsoluteURL; ?>layout/images/patientThree.jpg" />
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div>
			<?php require_once("tehPHP/logoCarousel.php");?>
		</div>
	</div>
</div>



<div class="mainPageDirectionsShell">
	<div class="centerWrap">
		<center>
			<div class="mainTitleSmall centerText">
				Discover Our <b>Dental Services</b>
			</div>
		</center>
		<div class="gridShellThirtyThree" style="grid-template-columns: repeat(auto-fit, minmax(250px, 1fr));">
			<div class="gridColumnShell">
				<a href="<?php echo $tehAbsoluteURL; ?>preventativeDentistry/" class="dentalServiceIconShell">
					<div class="centerText">
						<img width="90" alt="" title="" src="<?php echo $tehAbsoluteURL; ?>layout/icons/dentalXrayIcon.png" />
					</div>
					<div class="dentsalServiceTitle">
						Preventative
					</div>
				</a>
			</div>
			<div class="gridColumnShell">
				<a href="<?php echo $tehAbsoluteURL; ?>restorativeDentistry/" class="dentalServiceIconShell">
					<div class="centerText">
						<img width="90" alt="" title="" src="<?php echo $tehAbsoluteURL; ?>layout/icons/dentalPreventionIcon.png" />
					</div>
					<div class="dentsalServiceTitle">
						Restorative
					</div>
				</a>
			</div>
			<div class="gridColumnShell">
				<a href="<?php echo $tehAbsoluteURL; ?>cosmeticDentistry/" class="dentalServiceIconShell">
					<div class="centerText">
						<img width="90" alt="" title="" src="<?php echo $tehAbsoluteURL; ?>layout/icons/dentalWhiteningIcon.png" />
					</div>
					<div class="dentsalServiceTitle">
						Cosmetic
					</div>
				</a>
			</div>
			<div class="gridColumnShell">
				<a href="<?php echo $tehAbsoluteURL; ?>dentalImplants/" class="dentalServiceIconShell">
					<div class="centerText">
						<img width="90" alt="" title="" src="<?php echo $tehAbsoluteURL; ?>layout/icons/dentalImplantsIcon.png" />
					</div>
					<div class="dentsalServiceTitle">
						Implants
					</div>
				</a>
			</div>
		</div>
		<div style="padding: 0 20px;">

			<div class="dentActionButtonShell centerText">
				<a class="dentGreenButton" href="<?php echo $tehAbsoluteURL; ?>dentalServices.php">
					View More Services
				</a>
			</div>
		</div>
	</div>
</div>
	

<div class="dentistBG whiteText">
	<div class="centerWrap">
		<div class="gridShell">
			<div class="gridColumnShell">
				<div class="mainTitleSmall">
					Schedule <b>Your Appointment</b> Online
					<div class="leftTitleUnderline"></div>
				</div>
				<div class="footerSubtitle">
					New Patients Welcome!
				</div>
				<p>
					It's easy to book online with our real time scheduler which will provide you availability for both our Manhattan and Brooklyn locations.
				</p>
			</div>
			<div class="gridColumnShell">
				<div class="whiteShadowShell whiteBG stdBoxShadowOnColorBG">
					<div class="dentActionButtonShell centerText">
						<center>
							<div class="mainTitleSmall centerText blackText" style="font-weight: 600; font-size: 2.5em; position: relative; top -200px; border: none;">
								Schedule Online
							</div>
						</center>
						<br />
						<a class="dentGreenButton" href="https://www.zocdoc.com/practice/dr-inna-chern-dds-11530">
							Schedule Now!
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="mainPageDirectionsShell">
	<div class="centerWrap">
		<div class="dentalInsuranceShell lightGrayBG">
			<div class="dentalInsurnaceTitle centerText">
				We are here to help
			</div>
			<p class="grayText">
				We work with All major PPO insurance plans and credit cards. We offer flexible payment plans so that you and your family can get all your dental needs addressed.
			</p>
			<p class="grayText">
				We will verify your insurance prior to your appointment so that we can answer any questions prior to treatment. We will require a picture of the front and the back of the insured individuals card in order to get the benefits information. Feel free to also call the office with any questions.
			</p>
			<div class="row">
				<div class="gridShellThirtyThree" style="grid-template-columns: repeat(auto-fit, minmax(250px, 1fr));">
					<div class="insuranceLogoColumnShell">
						<img title="We Accept Aetna Insurance" alt="Aetna Insurance Logo" width="200" src="<?php echo $tehAbsoluteURL; ?>layout/logos/aetnaLogo.png" />
					</div>
					<div class="insuranceLogoColumnShell">
						<img title="We Accept Delta Insurance" alt="Delta Insurance Logo" width="200" src="<?php echo $tehAbsoluteURL; ?>layout/logos/deltaLogo.png" />
					</div>
					<div class="insuranceLogoColumnShell">
						<img title="We Accept Metlife Insurance" alt="Metlife Insurance Logo" width="200" src="<?php echo $tehAbsoluteURL; ?>layout/logos/metlifeLogo.png" />
					</div>
				</div>
			</div>
			<div class="row">
				<div class="gridShellThirtyThree" style="grid-template-columns: repeat(auto-fit, minmax(250px, 1fr));">
					<div class="insuranceLogoColumnShell">
						<img title="We Accept Cigna Insurance" alt="Cigna Insurance Logo" height="120" src="<?php echo $tehAbsoluteURL; ?>layout/logos/cignaLogo.png" />
					</div>
					<div class="insuranceLogoColumnShell">
						<img title="We Accept Guardian Insurance" alt="Guardian Insurance Logo" height="120" src="<?php echo $tehAbsoluteURL; ?>layout/logos/guardianLogo.png" />
					</div>
				</div>
			</div>
		</div>
	</div> 
</div>


<div class="mainPageDirectionsShell">
	<div class="centerWrap">
		<center>
			<div class="mainTitle centerText">
				Two Convenient Locations
			</div>
		</center>
		<br /><br />
		<div class="gridShell">
			<div class="gridColumnShell">
				<div class="contentSubTitle">
					Manhattan Office:
				</div>
				<div>
					<i class="fas fa-address-card"></i> 150 E 58th St, 8th Floor Annex, New York, NY 10155
				</div>
				<p class="greyText">
					We are conveniently located 2 blocks from the 4/5/6/Q/N/R 59th street station and 6 blocks from the E/M/F 51st street station.
				</p>
			</div>
			<div class="gridColumnShell">
				<div class="contentSubTitle">
					Brooklyn Office:
				</div>
				<div>
					<i class="fas fa-address-card"></i> 1854 Nostrand Ave, Brooklyn, NY 11226
				</div>
				<p class="greyText">
					We are located outside the Newkirk station on the 2/5 train and street parking is available daily.
				</p>
			</div>
		</div>
	</div>
</div>

<style>
</style>



<?php
	//if (substr_count(dirname($_SERVER['PHP_SELF']), '/') == "1")
	if( (substr_count(dirname($_SERVER['PHP_SELF']), '/') == "1") || (dirname($_SERVER['PHP_SELF']) == "/"))
	{
		require_once("./tehPHP/dentFooter.php");
	}
	else
	{
		require_once("../tehPHP/dentFooter.php");
	}
?>