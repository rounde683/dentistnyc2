<nav>
    <ul class="mainMenu">
        <li><a href="<?php echo $tehAbsoluteURL; ?>">Home</a></li>
        <li><a href="<?php echo $tehAbsoluteURL; ?>testimonials.php">Dental Services</a></li>
        <li><a href="<?php echo $tehAbsoluteURL; ?>testimonials.php">Testimonials</a></li>
        <li><a href="<?php echo $tehAbsoluteURL; ?>blog/">Blog</a></li>
        <li><a href="<?php echo $tehAbsoluteURL; ?>about.php">About</a></li>
        <li><a href="<?php echo $tehAbsoluteURL; ?>contact.php">Contact</a></li>
    </ul>
    <div class="button">
        <a class="btn-open" href="#"></a>
    </div>
</nav>
<div class="overlay">
    <div class="wrap">
        <ul class="wrap-nav">
            <li><a href="#">About</a>
            <ul>
                <li><a href="#">About Company</a></li>
                <li><a href="#">Designers</a></li>
                <li><a href="#">Developers</a></li>
                <li><a href="#">Pets</a></li>
            </ul>
            </li>
            <li><a href="#">Services</a>
            <ul>
                <li><a href="https://www.google.hr/">Web Design</a></li>
                <li><a href="#">Development</a></li>
                <li><a href="#">Apps</a></li>
                <li><a href="#">Graphic design</a></li>
                <li><a href="#">Branding</a></li>
            </ul>
            </li>
            <li><a href="#">Work</a>
            <ul>
                <li><a href="#">Web</a></li>
                <li><a href="#">Graphic</a></li>
                <li><a href="#">Apps</a></li>
            </ul>
            </li>
        </ul>

<ul class="footerSocialMenuShell centerText">
                    <li>
                        <a href="https://www.facebook.com/innacherndds/" title="Like Us on Facebook!">
                            <i class="fab fa-facebook-f"></i>
                        </a>
                    </li>
                    <li>
                        <a href="http://instagram.com/innacherndds" title="Check us out on Instagram!">
                            <i class="fab fa-instagram"></i>
                        </a>
                    </li>
                    <li>
                        <a href="https://www.linkedin.com/in/inna-chern-5787413b" title="See Inna's Linked In Credentials &amp; Accolade">
                            <i class="fab fa-linkedin-in"></i>
                        </a>
                    </li>
                    <li>
                        <a href="mailto:innacherndds@gmail.com" title="Email Us with your Questions!">
                            <i class="fas fa-envelope"></i>
                        </a>
                    </li>
                </ul>
                <div class="footerCopyRight">
                    &copy; <?php echo date("Y");?> - All rights are reserved
                </div>

        <div class="social">
            <a href="https://www.facebook.com/innacherndds/">
                <div class="social-icon">
                    <i class="fa fa-facebook"></i>
                </div>
            </a>
            <a href="http://instagram.com/innacherndds">
                <div class="social-icon">
                    <i class="fab fa-instagram"></i>
                </div>
            </a>
            <a href="https://www.linkedin.com/in/inna-chern-5787413b">
                <div class="social-icon">
                    <i class="fab fa-linkedin-in"></i>
                </div>
            </a>
            <a href="mailto:innacherndds@gmail.com">
                <div class="social-icon">
                    <i class="fas fa-envelope"></i>
                </div>
            </a>
        </div>
    </div>
</div>

<link rel="stylesheet" href="<?php echo $tehAbsoluteURL; ?>css/overlayMenuFS.css">
<script  src="<?php echo $tehAbsoluteURL; ?>js/overlayMenuFS.js"></script>