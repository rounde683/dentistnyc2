<style>
	.footerShellShell{padding: 50px 0 0 0; color: #FFFFFF;}
	.preFooterShell{padding: 50px 3%;}
	.preFooterText{color: #FFFFFF; font-size: 1.5em;}
	.footerSubtitle{font-size: .9em; margin-top: 20px;}
	.footerTitle{font-size: 1.2em;}
	.footerShell{padding: 50px 0;}
	.blackBG{background: #2B2B2B;}
	.footerSocialMenuShell{margin: 0; padding: 0;list-style: none; font-size: 1.2em; text-align: center;}
	.footerSocialMenuShell>li{display: inline;}
	.footerSocialMenuShell>li>a{color: #FFFFFF; margin: 0 10px; text-decoration: none; transition: all .3s ease;}
	.footerSocialMenuShell>li>a:hover{color: #0097b6; font-size: 2em;}
	.footerCopyRight{padding: 15px 0; font-size: .9em; color: #CDCDCD;}
	.footerList{list-style-type: none; margin: 0;}
	.footerGray{color: #989898;}
	.footerQuote{font-size: .8em;}
	.footerInput{padding: 3px 5px; background: #CDCDCD; width: 90%; margin: 20px auto;}
</style>
<div class="footerShell blackBG">
	<div class="gridShellThirtyThree" style="grid-template-columns: repeat(auto-fit, minmax(250px, 1fr));">
		<div class="gridColumnShell">
			<div class="footerTitle whiteText centerText">
				<i class="far fa-clock"></i> <span class="blueText">Office Hours</span>
			</div>
			<div class="footerSubtitle footerGray">
				Manhattan Location
			</div>
			<ul class="footerList whiteText">
				<li>Mon 8am - 7pm</li>
				<li>Thur 8am - 7pm</li>
			</ul>
			<div class="footerSubtitle footerGray">
				Brooklyn Location
			</div>
			<ul class="footerList whiteText">
				<li>Weds 10am - 7pm</li>
				<li>Fri 10am - 2pm</li>
				<li>Sat 9am - 3pm</li>
			</ul>
		</div>
		<div class="gridColumnShell">
			<div class="footerTitle whiteText centerText">
				<i class="fas fa-map-marker-alt"></i> <span class="blueText">Contact Us</span>
			</div>
			<div class="footerSubtitle footerGray">
				Manhattan Location
			</div>
			<ul class="footerList whiteText">
				<li><i class="fas fa-phone"></i> (212) 838-0842</li>
				<li><i class="fas fa-envelope"></i> 964dental@gmail.com</li>
			</ul>
			<div class="footerSubtitle footerGray">
				Brooklyn Location
			</div>
			<ul class="footerList whiteText">
				<li><i class="fas fa-phone"></i>  (718) 282-4142</li>
				<li><i class="fas fa-envelope"></i> icherndds@gmail.com</li>
			</ul>
		</div>
		<div class="gridColumnShell">
			<div class="footerTitle whiteText centerText">
				<i class="far fa-comments"></i> <span class="blueText">Follow Us</span>
			</div>
			<br /><br />
			<div class="footerSocialShell centerText">
				<ul class="footerSocialMenuShell centerText">
					<li>
						<a href="https://www.facebook.com/innacherndds/" title="Like Us on Facebook!">
							<i class="fab fa-facebook-f"></i>
						</a>
					</li>
					<li>
						<a href="http://instagram.com/innacherndds" title="Check us out on Instagram!">
							<i class="fab fa-instagram"></i>
						</a>
					</li>
					<li>
						<a href="https://www.linkedin.com/in/inna-chern-5787413b" title="See Inna's Linked In Credentials &amp; Accolade">
							<i class="fab fa-linkedin-in"></i>
						</a>
					</li>
					<li>
						<a href="mailto:innacherndds@gmail.com" title="Email Us with your Questions!">
							<i class="fas fa-envelope"></i>
						</a>
					</li>
				</ul>
				<br />
				<ul class="footerList whiteText">
					<li><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i></li>
				</ul>
				<div class="footerQuote whiteText">
					"Inna Chern DDS is rated 5 out of 5 stars based on 55 Google reviews from around the Web"
				</div>
			</div>
		</div>
		<div class="gridColumnShell">
			<div class="footerTitle whiteText centerText">
				<i class="far fa-clock"></i> <span class="blueText">Mailing List</span>
			</div>
			<input class="footerInput" type="text" name="email" value="" placeholder="email@email.com" />
			<center>
				<a class="dentGreenButton" href="https://www.zocdoc.com/practice/dr-inna-chern-dds-11530">
					Subscribe
				</a>
			</center>
		</div>
	</div>
	<br />
	<div class="footerCopyRight centerText">
		&copy; <?php echo date("Y");?> - All rights are reserved
	</div>
</div>
	</body>
</html>