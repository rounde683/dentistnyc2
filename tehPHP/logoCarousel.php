<script type="text/javascript" src="<?php echo $tehAbsoluteURL; ?>js/jquery.bxslider.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo $tehAbsoluteURL; ?>css/jquery.bxslider.css" />
<script type="text/javascript">
	$(document).ready(function(){
	  $('.logoCarousel').bxSlider({
	  	//mode: 'fade',
	  	adaptiveHeight: true,
	    minSlides: 1,
	    auto: true,
	    maxSlides: 1
	  });
	});
</script>
<style type="text/css">
	#logoCarouselShell{height: 350px !important;}
	.bx-viewport{background: none !important; height: 280px !important;}
	.revTechTestimonialSlideShell{padding: 0 60px;}
	.revTechTestimonialText{font-style: italic; color: #686868; font-size: 1.3em; }
	.revTechTestimonialTextAuthor{color: #686868; font-size: .9em; padding-right: 20px; text-align: right;}
</style>
<div id="logoCarouselShell">
	<div class="logoCarousel">
		<div class="slide revTechTestimonialSlideShell">
			<p class="testimonialText">
			<div class="revTechTestimonialText">
				"I have been Dr.Chern's patient for several years now and I have no intention of ever finding another dentist. She was my primary dentist when i lived in NY, but I have since moved to DE and PA , and will travel up to see her for my appointments. When you find an amazing dentist, you stick with them. She's honest, professional, delicate and knows what she's doing. Her staff is just as amazing.""
			</div>
			<div class="revTechTestimonialTextAuthor">
				 - David K. (Google Reviews)
			</div>
		</div>
		<div class="slide revTechTestimonialSlideShell">
			<div class="revTechTestimonialText">
				"Amazing experience start to finish. Nicest and most professional doctor and office. Modern facility, clean, quiet and no wait. High recommend!!!""
			</div>
			<div class="revTechTestimonialTextAuthor">
				  - Victoria T. (Google Reviews)
			</div>
		</div>
		<div class="slide revTechTestimonialSlideShell">
			<div class="revTechTestimonialText">
				"Great doctor! Very attentive and careful. Extremely knowledgeable. I never thought dental work will be as easy and smooth. Thank you Inna!!""
			</div>
			<div class="revTechTestimonialTextAuthor">
				  - Ahmed K (Google Reviews)
			</div>
		</div>
		<div class="slide revTechTestimonialSlideShell">
			<div class="revTechTestimonialText">
				"Been a patient of Dr. Chern since 2001 and will be until I have no more teeth or I'm dead. Whichever comes first. She's simply the best. Bedside manner A+, knowing her shit A+, and always makes a pleasant experience out of an always unpleasant dentist experience. This is a no brainer people. Fire your current dentist and run to see her"
			</div>
			<div class="revTechTestimonialTextAuthor">
				  - Russ W. (Yelp)
			</div>
		</div>
		<div class="slide revTechTestimonialSlideShell">
			<div class="revTechTestimonialText">
				"I don't like going to the dentist period!  Dr. Chern puts you st ease.  Very friendly, professional and zero wait time!  Also, very thorough and attentive. Highly recommend!""
			</div>
			<div class="revTechTestimonialTextAuthor">
				  - Irina P. (Yelp)
			</div>
		</div>
		<div class="slide revTechTestimonialSlideShell">
			<div class="revTechTestimonialText">
				"Dr. Chern was very professional and also, what I loved, very understanding of the financial issues associated with cosmetic dentistry.  She is attentive and professional, but does not try to push you into unnecessary costly work like I've had other dentists do.  The office staff were also very helpful and courteous.  All around excellent experience.""
			</div>
			<div class="revTechTestimonialTextAuthor">
				   - Erin N. (Yelp)
			</div>
		</div>
	</div>
</div>