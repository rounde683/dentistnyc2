<?php $tehAbsoluteURL = "http://www.dentistnyc2.com/"; ?>
<?php $tehAbsoluteURL = "http://localhost/dentistnyc2/"; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"	xml:lang="en">
	<head>
		<title><?php echo $pageTitle; ?></title>
		
		<script src="<?php echo $tehAbsoluteURL; ?>js/jquery-3.2.1.min.js" type="text/javascript" charset="utf-8"></script>
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
		<link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">

        <link rel="stylesheet" type="text/css" href="<?php echo $tehAbsoluteURL; ?>css/default.css" />

		<link rel="stylesheet" type="text/css" href="<?php echo $tehAbsoluteURL; ?>css/font-awesome_min.css" />

		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta http-equiv="Content-Type"	content="text/html; charset=iso-8859-1" />

		<meta name='keywords' content="<?php echo $pageKeywords; ?>" />
		<meta name='description' content="<?php echo $pageDesc; ?>" />

		<link rel="SHORTCUT ICON" href="<?php echo $tehAbsoluteURL; ?>favicon.ico" />


		<link rel="apple-touch-icon" sizes="57x57" href="<?php echo $tehAbsoluteURL; ?>apple-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="60x60" href="<?php echo $tehAbsoluteURL; ?>apple-icon-60x60.png">
		<link rel="apple-touch-icon" sizes="72x72" href="<?php echo $tehAbsoluteURL; ?>apple-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="76x76" href="<?php echo $tehAbsoluteURL; ?>apple-icon-76x76.png">
		<link rel="apple-touch-icon" sizes="114x114" href="<?php echo $tehAbsoluteURL; ?>apple-icon-114x114.png">
		<link rel="apple-touch-icon" sizes="120x120" href="<?php echo $tehAbsoluteURL; ?>apple-icon-120x120.png">
		<link rel="apple-touch-icon" sizes="144x144" href="<?php echo $tehAbsoluteURL; ?>apple-icon-144x144.png">
		<link rel="apple-touch-icon" sizes="152x152" href="<?php echo $tehAbsoluteURL; ?>apple-icon-152x152.png">
		<link rel="apple-touch-icon" sizes="180x180" href="<?php echo $tehAbsoluteURL; ?>apple-icon-180x180.png">
		<link rel="icon" type="image/png" sizes="192x192"  href="<?php echo $tehAbsoluteURL; ?>android-icon-192x192.png">
		<link rel="icon" type="image/png" sizes="32x32" href="<?php echo $tehAbsoluteURL; ?>favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="96x96" href="<?php echo $tehAbsoluteURL; ?>favicon-96x96.png">
		<link rel="icon" type="image/png" sizes="16x16" href="<?php echo $tehAbsoluteURL; ?>favicon-16x16.png">
		<link rel="manifest" href="<?php echo $tehAbsoluteURL; ?>manifest.json">
		<meta name="msapplication-TileColor" content="#ffffff">
		<meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
		<meta name="theme-color" content="#ffffff">

		<script>
			$(document).ready(function() {
			});
		</script>
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-121049933-1"></script>
		<script>
			window.dataLayer = window.dataLayer || [];
			function gtag(){dataLayer.push(arguments);}
			gtag('js', new Date());

			gtag('config', 'UA-121049933-1');
		</script>


	</head>
	<body>
		

<link rel="stylesheet" href="<?php echo $tehAbsoluteURL; ?>css/overlayMenuFS.css">
<script  src="<?php echo $tehAbsoluteURL; ?>js/overlayMenuFS.js"></script>
			<?php
				if( (substr_count(dirname($_SERVER['PHP_SELF']), '/') == "1") || (dirname($_SERVER['PHP_SELF']) == "/"))
				//if(dirname($_SERVER['PHP_SELF']) == "/")
				{
					require_once("./tehPHP/mainMenu.php");
				}
				else
				{
					require_once("../tehPHP/mainMenu.php");
				}
			?>
		</div>