
<div class="mainMenuShell">
            <nav>
                <span class="mainMenuLogoShell">
                    <a href="<?php echo $tehAbsoluteURL; ?>"><img class="mainLogoIMG" alt="Dr. Inna Chern DDS" src="<?php echo $tehAbsoluteURL; ?>layout/logos/DrInnaChernDDSLogo.png" title="Brightening the smiles of NYC"/></a>
                </span>
                <ul class="mainMenu">
                    <li><a href="<?php echo $tehAbsoluteURL; ?>dentalServices.php">Dental Services</a></li>
                    <li><a href="<?php echo $tehAbsoluteURL; ?>aboutUs.php">About</a></li>
                    <li><a href="<?php echo $tehAbsoluteURL; ?>contactUs.php">Contact</a></li>
                </ul>
                <div class="button">
                    <a class="btn-open" href="#"></a>
                </div>
            </nav>
            <div class="overlay">
                <div class="wrap">
                    <ul class="wrap-nav">
                        <li><a href="#">Home</a>
                        <ul>
                            <li><a href="<?php echo $tehAbsoluteURL; ?>aboutUs.php">About</a></li>
                            <li><a href="<?php echo $tehAbsoluteURL; ?>contactUs.php">Contact</a></li>
                            <li><a href="<?php echo $tehAbsoluteURL; ?>dentalServices.php">Dental Services Overview</a></li>
                        </ul>
                        </li>
                        <li><a href="<?php echo $tehAbsoluteURL; ?>/blog/">Blog</a>
                        <ul>
                            <li><a href="http://www.dentistnyc2.com/blog/a-great-alternative-to-flippers-during-implant-treatment">Alternatives to Flippers</a></li>
                            <li><a href="http://www.dentistnyc2.com/blog/clear-braces-and-bisphosphonates">Clear Braces</a></li>
                            <li><a href="http://www.dentistnyc2.com/blog/a-drill-free-approach-to-getting-rid-of-white-spots-in-teeth">Be rid of White Spots 3</a></li>
                        </ul>
                        </li>
                        <li><a href="<?php echo $tehAbsoluteURL; ?>preventativeDentistry/">Preventative Dentistry</a>
                        <ul>
                            <li>
                                <a href="<?php echo $tehAbsoluteURL; ?>preventativeDentistry/dentalExamAndCleaning.php">
                                     Dental Exam &amp; Cleaning
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo $tehAbsoluteURL; ?>preventativeDentistry/oralCancerScreenings.php">
                                    Oral Cancer Screening
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo $tehAbsoluteURL; ?>preventativeDentistry/pediatricDentalTreatments.php">
                                    Pediatric Dental Treatments
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo $tehAbsoluteURL; ?>preventativeDentistry/nightAndSportsGuards.php">
                                    Night &amp; Sport Guards
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo $tehAbsoluteURL; ?>preventativeDentistry/periodontalTreatments.php">
                                    Periodontal Treatments
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo $tehAbsoluteURL; ?>preventativeDentistry/dentalEmergencyCare.php">
                                    Dental Emergency Care
                                </a>
                            </li>
                        </ul>
                        </li>
                        <li><a href="<?php echo $tehAbsoluteURL; ?>restorativeDentistry/">Restorative</a>
                            <ul>
                                <li>
                                    <a href="<?php echo $tehAbsoluteURL; ?>restorativeDentistry/toothColoredFillings.php">
                                          Tooth Colored Fillings
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo $tehAbsoluteURL; ?>restorativeDentistry/crownsAndBridges.php">
                                        Crowns &amp; Bridges
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo $tehAbsoluteURL; ?>restorativeDentistry/rootCanals.php">
                                        Root Canals
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo $tehAbsoluteURL; ?>restorativeDentistry/extractions.php">
                                        Extractions
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo $tehAbsoluteURL; ?>restorativeDentistry/dentures.php">
                                        Dentures
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li><a href="<?php echo $tehAbsoluteURL; ?>cosmeticDentistry/">Cosmetic Dentistry</a>
                            <ul>
                                <li>
                                    <a href="<?php echo $tehAbsoluteURL; ?>cosmeticDentistry/bonding.php">
                                        Bonding
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo $tehAbsoluteURL; ?>cosmeticDentistry/porcelainVeneers.php">
                                        Veneers
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo $tehAbsoluteURL; ?>cosmeticDentistry/porcelainCrowns.php">
                                        Porcelain Crowns &amp; Bridges
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo $tehAbsoluteURL; ?>cosmeticDentistry/invisalign.php">
                                        Invisalign
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo $tehAbsoluteURL; ?>cosmeticDentistry/whitening.php">
                                        Whitening Treatments
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li><a href="<?php echo $tehAbsoluteURL; ?>dentalImplants">Implant</a>
                            <ul>
                                <li>
                                    <a href="<?php echo $tehAbsoluteURL; ?>dentalImplants/dentalImplants.php">
                                        Implants
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo $tehAbsoluteURL; ?>dentalImplants/implantCrowns.php">
                                        Implant Crowns
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo $tehAbsoluteURL; ?>dentalImplants/implantBridges.php">
                                        Implant Bridges
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo $tehAbsoluteURL; ?>dentalImplants/retainedDentures.php">
                                        Implant Retained Dentures
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                    <div class="social">
                        <a href="https://www.facebook.com/innacherndds/">
                            <div class="social-icon">
                                <i class="fa fa-facebook"></i>
                            </div>
                        </a>
                        <a href="http://instagram.com/innacherndds">
                            <div class="social-icon">
                                <i class="fab fa-instagram"></i>
                            </div>
                        </a>
                        <a href="https://www.linkedin.com/in/inna-chern-5787413b">
                            <div class="social-icon">
                                <i class="fab fa-linkedin-in"></i>
                            </div>
                        </a>
                        <a href="mailto:innacherndds@gmail.com">
                            <div class="social-icon">
                                <i class="fas fa-envelope"></i>
                            </div>
                        </a>
                    </div>
                </div>
            </div>